FROM elixir:alpine as common

ENV ROOT=/aether USER=aether GROUP=users

RUN apk add git && \
  adduser -S -G ${GROUP} ${USER} && \
  mkdir ${ROOT} && \
  chown -R ${USER}:${GROUP} ${ROOT}

USER ${USER}
WORKDIR ${ROOT}

RUN mix local.hex --force && \
  mix local.rebar --force

COPY mix.* ${ROOT}/
COPY config ${ROOT}/config

CMD iex -S mix

FROM common as build-env

ARG MIX_ENV=test

RUN mix deps.get && mix deps.compile

COPY --chown=aether:users . ${ROOT}

RUN mix compile

FROM common as production-env

ENV MIX_ENV=production

RUN mix deps.get && mix deps.compile

COPY --chown=aether:users . ${ROOT}

RUN mix compile
