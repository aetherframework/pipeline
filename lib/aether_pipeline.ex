defmodule Aether.Pipeline do
  @moduledoc """
  This is the Pipeline module
  """

  alias __MODULE__

  @callback definition(opts :: term()) :: term()

  @type id :: String.t()

  @pipelines_supervisor Pipeline.PipelinesSupervisor

  defstruct [
    :id,
    :pid,
    :ref,
    :owner
  ]

  defmacro __using__(_opts) do
    quote location: :keep, bind_quoted: [module: __CALLER__.module] do
      alias Aether.Pipeline
      alias Aether.Pipeline.Builder

      def build(opts \\ []),
        do: opts |> definition() |> Builder.build()

      def start(opts \\ [], owner \\ nil),
        do: opts |> build() |> Pipeline.start(owner)

      def run(opts \\ []),
        do: opts |> start(self()) |> Pipeline.await()
    end
  end

  def start(spec),
    do: start(spec, nil, gen_pipeline_id())

  def start(spec, owner) when is_pid(owner) or is_nil(owner),
    do: start(spec, owner, gen_pipeline_id())

  def start(spec, owner, id) when is_pid(owner) or is_nil(owner) do
    with {:ok, pid} <- start_pipeline(spec, owner, id) do
      ref = Process.monitor(pid)

      %__MODULE__{
        id: id,
        pid: pid,
        ref: ref,
        owner: self()
      }
    end
  end

  def await(%__MODULE__{ref: ref} = pipeline) do
    receive do
      {:"$aether_pipeline:done", result} ->
        Process.demonitor(ref, [:flush])
        {:ok, result}

      {:DOWN, ^ref, _, proc, reason} ->
        {:error, reason(reason, proc), {__MODULE__, :await, [pipeline]}}
    end
  end

  defp gen_pipeline_id, do: Nanoid.generate(12)

  defp start_pipeline(spec, owner, id) do
    {Pipeline.Supervisor, spec: spec, pipeline_id: id, owner: owner}
    |> Supervisor.child_spec(restart: :transient)
    |> start_supervised()
  end

  defp start_supervised(child_spec) do
    DynamicSupervisor.start_child(@pipelines_supervisor, child_spec)
  end

  defp reason(:noconnection, proc), do: {:nodedown, monitor_node(proc)}
  defp reason(reason, _), do: reason

  defp monitor_node(pid) when is_pid(pid), do: node(pid)
  defp monitor_node({_, node}), do: node
end
