defmodule Aether.Pipeline.Graph do
  @moduledoc """
  Pipeline network graph
  """

  alias Graph.Reducers.Bfs

  def new, do: Graph.new()

  def new(components: components),
    do: new() |> add_components(components)

  @doc """
  Checks if the graph is a valid pipeline graph. Validity is determined
  by checking the that graph is acyclic and that all sources have
  operator/sink outputs with no inputs and all sinks have source/operator
  inputs with no outputs.

  ## Examples

      iex> graph = Aether.Pipeline.Graph.new()
      iex> Aether.Pipeline.Graph.valid?(graph)
      true

  """
  def valid?(%Graph{} = graph), do: Graph.is_acyclic?(graph)

  def component_outputs(%Graph{} = graph),
    do: vertex_outputs(graph)

  def component_links(%Graph{} = graph) do
    graph
    |> component_outputs()
    |> Enum.flat_map(fn {from, outs} -> Enum.map(outs, &{&1, from}) end)
  end

  defp add_components(graph, components),
    do: Enum.reduce(components, graph, &add_component(&2, &1))

  defp add_component(graph, %{path: path, inputs: inputs} = component) do
    labels = component_labels(component)
    graph = add_vertex(graph, path, labels)

    Enum.reduce(inputs, graph, &add_edge(&2, &1, path))
  end

  defp add_vertex(graph, key, labels),
    do: Graph.add_vertex(graph, key, labels)

  defp add_edge(%Graph{} = graph, from, to),
    do: Graph.add_edge(graph, from, to)

  defp component_labels(%{name: name, type: type}), do: [name, type]

  defp vertex_outputs(graph) do
    graph
    |> Bfs.map(fn v -> {v, Graph.out_neighbors(graph, v)} end)
    |> Enum.filter(fn {_, out} -> length(out) > 0 end)
  end
end
