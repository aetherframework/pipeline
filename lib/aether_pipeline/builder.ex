defmodule Aether.Pipeline.Builder do
  @moduledoc """
  Pipeline Config Builder
  """

  alias Aether.Pipeline.{Channel, Config, Graph, Message, Spec}

  def build(%{name: name, stages: _stages} = opts) do
    config = build_config(opts)
    components = build_stages(opts)
    graph = Graph.new(components: components)

    Spec.new(
      name: name,
      graph: graph,
      config: config,
      components: components
    )
  end

  defp build_config(opts),
    do: opts |> Map.get(:config, []) |> Enum.into(%{}) |> Config.new()

  defp build_stages(opts),
    do: opts |> Map.get(:stages, []) |> Enum.flat_map(&build_stage/1)

  defp build_stage({name, opts}) do
    opts = Map.put(opts, :name, name)
    {:ok, stage_config} = opts.module.build(opts)

    channel_configs = build_channels(stage_config)

    [stage_config | channel_configs]
  end

  defp build_channels(%{channels: %{}}), do: []

  defp build_channels(%{name: input_name, path: input_path, channels: channels}) do
    Enum.map(channels, &build_channel(input_name, input_path, &1))
  end

  defp build_channel(input_name, input_path, %{name: name} = opts) do
    selector = fn
      %Message{errors: errs} when length(errs) != 0 -> false
      %Message{meta: %{channel: channel}} -> channel == name
      %Message.Batch{meta: %{channel: channel}} -> channel == name
      %Message.Control{} -> true
      _ -> false
    end

    {:ok, config} =
      opts
      |> Map.put(:name, :"#{input_name}/#{name}")
      |> Map.put_new(:path, "channel/#{input_name}/#{name}")
      |> Map.put_new(:inputs, [input_path])
      |> Map.put_new(:module, Channel)
      |> Map.put_new(:dispatcher, :demand)
      |> Map.put_new(:selector, selector)
      |> Map.put_new(:stages, 1)
      |> Map.put_new(:channels, [])
      |> Channel.build()

    config
  end
end
