defmodule Aether.Pipeline.Spec do
  @moduledoc """
  Pipeline Spec
  """

  alias Aether.Pipeline.Component

  @derive {Inspect, only: [:name]}

  defstruct [
    :name,
    :graph,
    :config,
    :components,
    :pipeline_id
  ]

  def new(name: name, graph: graph, config: cfg, components: comps),
    do: %__MODULE__{name: name, graph: graph, config: cfg, components: comps}

  def component_specs(%__MODULE__{components: components}, plid),
    do: Enum.map(components, &Component.component_spec(&1, plid))
end
