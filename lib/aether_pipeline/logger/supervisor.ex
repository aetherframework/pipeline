defmodule Aether.Pipeline.Logger.Supervisor do
  @moduledoc """
  Pipline logger supervisor
  """

  alias Aether.Core.Service.Resolver
  alias Aether.Pipeline.Logger

  use Supervisor

  import Resolver.Process

  def start_link([spec: _spec, pipeline_id: plid] = opts) do
    name = via_service_group(plid, ~p/logger supervisor/)

    Supervisor.start_link(__MODULE__, opts, name: name)
  end

  def init(spec: spec, pipeline_id: plid) do
    service_group_set(plid)

    children = [{Logger.Server, spec: spec, pipeline_id: plid}]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
