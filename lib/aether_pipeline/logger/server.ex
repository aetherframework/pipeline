defmodule Aether.Pipeline.Logger.Server do
  @moduledoc """
  Pipline logger server
  """

  alias Aether.Core.PubSub
  alias Aether.Core.Service.Resolver
  alias Aether.Pipeline.Logger.State

  use GenServer, restart: :transient, shutdown: 10_000

  require Logger

  import Resolver.Process

  @default_log_data %{level: :info, metadata: []}

  def start_link([spec: _spec, pipeline_id: plid] = opts) do
    name = via_service_group(plid, ~p/logger/)

    GenServer.start_link(__MODULE__, opts, name: name)
  end

  def init(spec: %{config: config}, pipeline_id: plid) do
    service_group_set(plid)

    state = State.new(level: config.logger.level, pipeline_id: plid)

    {:ok, state, {:continue, :setup_pubsub}}
  end

  def handle_continue(:setup_pubsub, state) do
    with {:ok, pubsub} <- resolve_service(~p/pubsub/) do
      PubSub.subscribe(pubsub, :log, {:send, :log})

      Logger.info("[pipeline] logger initialized")
    end

    {:noreply, state}
  end

  def handle_info({:log, %{payload: payload}}, state) do
    @default_log_data
    |> Map.merge(payload)
    |> do_log(state)

    {:noreply, state}
  end

  defp do_log(%{level: :debug} = log, %State{level: :debug}),
    do: Logger.debug(log.message)

  defp do_log(%{level: :debug}, _state), do: :ok

  defp do_log(%{level: :info} = log, %State{level: :debug}),
    do: Logger.info(log.message)

  defp do_log(%{level: :info} = log, %State{level: :info}),
    do: Logger.info(log.message)

  defp do_log(%{level: :info}, _state), do: :ok

  defp do_log(%{level: :warn} = log, %State{level: :debug}),
    do: Logger.warn(log.message)

  defp do_log(%{level: :warn} = log, %State{level: :info}),
    do: Logger.warn(log.message)

  defp do_log(%{level: :warn} = log, %State{level: :warn}),
    do: Logger.warn(log.message)

  defp do_log(%{level: :warn}, _state), do: :ok

  defp do_log(%{level: :error} = log, _),
    do: Logger.error(log.message)
end
