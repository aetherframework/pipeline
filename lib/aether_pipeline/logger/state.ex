defmodule Aether.Pipeline.Logger.State do
  @moduledoc """
  Pipeline Logger state
  """

  defstruct [:level, :pipeline_id]

  def new(level: level, pipeline_id: plid),
    do: %__MODULE__{level: level, pipeline_id: plid}
end
