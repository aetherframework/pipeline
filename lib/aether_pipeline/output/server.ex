defmodule Aether.Pipeline.Output.Server do
  @moduledoc """
  Pipeline output collection server
  """

  alias Aether.Core.PubSub
  alias Aether.Core.Service.Resolver
  alias Aether.Pipeline.Logger
  alias Aether.Pipeline.Output.Data

  use GenServer, restart: :transient, shutdown: 10_000

  import Resolver.Process

  def start_link([spec: _, pipeline_id: plid] = opts) do
    name = via_service_group(plid, ~p/output/)

    GenServer.start_link(__MODULE__, opts, name: name)
  end

  def init(spec: _, pipeline_id: plid) do
    service_group_set(plid)

    {:ok, Data.new(), {:continue, :init_subscriptions}}
  end

  def get(pid, key),
    do: GenServer.call(pid, {:get, key})

  def get(pid, key, default),
    do: GenServer.call(pid, {:get, key, default})

  def dump(pid), do: GenServer.call(pid, :state)

  def put(pid, key, value),
    do: GenServer.cast(pid, {:put, key, value})

  def put_new(pid, key, value),
    do: GenServer.cast(pid, {:put_new, key, value})

  def update(pid, key, fun) when is_function(fun),
    do: GenServer.cast(pid, {:update, key, nil, fun})

  def update(pid, key, default, fun) when is_function(fun),
    do: GenServer.cast(pid, {:update, key, default, fun})

  def handle_continue(:init_subscriptions, output) do
    with {:ok, pubsub} <- resolve_service(~p/pubsub/) do
      setup_subscriptions(pubsub)
      publish_initialized(pubsub)

      Logger.debug("[output] initialized")
    end

    {:noreply, output}
  end

  def handle_cast({:put, key, value}, output),
    do: {:noreply, Data.put(output, key, value)}

  def handle_cast({:put_new, key, value}, output),
    do: {:noreply, Data.put_new(output, key, value)}

  def handle_cast({:update, key, default, fun}, output),
    do: {:noreply, Data.update(output, key, default, fun)}

  def handle_call({:get, key}, _from, output),
    do: {:reply, Data.get(output, key), output}

  def handle_call({:get, key, default}, _from, output),
    do: {:reply, Data.get(output, key, default), output}

  def handle_call(:state, _from, output),
    do: {:reply, output, output}

  def handle_info({:update, %{payload: payload}}, output) do
    output =
      case payload do
        %{key: key, default: default, update: fun} ->
          Data.update(output, key, default, fun)

        %{key: key, update: fun} ->
          Data.update(output, key, nil, fun)
      end

    {:noreply, output}
  end

  defp setup_subscriptions(pubsub) do
    subscriptions = [
      {"output:update", {:send, :update}}
    ]

    Enum.each(subscriptions, fn {topic, handler} ->
      PubSub.subscribe(pubsub, topic, handler)
    end)
  end

  defp publish_initialized(pubsub),
    do: PubSub.dispatch(pubsub, "output:init", nil)
end
