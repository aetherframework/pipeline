defmodule Aether.Pipeline.Output.Data do
  @moduledoc """
  Pipeline output
  """

  @derive [Inspect]

  defstruct data: %{}

  def new, do: %__MODULE__{data: %{}}
  def new(data), do: %__MODULE__{data: data}

  def get(%__MODULE__{data: data}, key),
    do: Map.get(data, key)

  def get(%__MODULE__{data: data}, key, default),
    do: Map.get(data, key, default)

  def put(%__MODULE__{data: data}, key, value),
    do: %__MODULE__{data: Map.put(data, key, value)}

  def put_new(%__MODULE__{data: data}, key, value),
    do: %__MODULE__{data: Map.put_new(data, key, value)}

  def update(%__MODULE__{data: data}, key, default, fun),
    do: %__MODULE__{data: Map.update(data, key, default, fun)}
end
