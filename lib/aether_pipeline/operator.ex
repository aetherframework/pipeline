defmodule Aether.Pipeline.Operator do
  @moduledoc """
  Pipeline Operator
  """

  alias Aether.Pipeline.{Message, Stage}

  @callback init(config :: term()) :: term()
  @callback call(message :: term(), state :: term(), config :: term()) :: term()
  @callback flush(state :: term(), config :: term()) :: {:ok, [term()], term()}

  defmacro __using__(opts) do
    quote location: :keep, bind_quoted: [opts: opts, module: __CALLER__.module] do
      alias Aether.Pipeline.{Component, Message, Operator, Stage}

      use Component, type: :operator, stage: Stage.Processor

      @behaviour Operator

      @impl Operator
      def init(_config), do: {:ok, %{}}

      @impl Operator
      def call(message, state, _config), do: {:ok, message, state}

      @impl Operator
      def flush(state, _config), do: {:ok, [], state}

      def process_message(message, state, config),
        do: call(message, state, config)

      defoverridable init: 1,
                     call: 3,
                     flush: 2
    end
  end
end
