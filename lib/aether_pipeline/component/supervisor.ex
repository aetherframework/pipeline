defmodule Aether.Pipeline.Component.Supervisor do
  @moduledoc """
  Component instance supervisor
  """

  alias Aether.Core.Service.Resolver
  alias Aether.Pipeline.{Component, Stage}

  use Supervisor

  import Resolver.Process

  def start_link([config: config, pipeline_id: plid] = opts) do
    name = via_service_group(plid, ~p/#{config.path} supervisor/)

    Supervisor.start_link(__MODULE__, opts, name: name)
  end

  def init(config: config, pipeline_id: plid) do
    service_group_set(plid)

    children = [
      {Stage.Supervisor, config: config, pipeline_id: plid},
      {Component.State, config: config, pipeline_id: plid},
      {Component.Server, config: config, pipeline_id: plid}
    ]

    Supervisor.init(children, strategy: :one_for_all)
  end
end
