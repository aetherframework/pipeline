defmodule Aether.Pipeline.Component.Server do
  @moduledoc """
  Component State Server
  """

  alias Aether.Core.PubSub
  alias Aether.Core.Service.Resolver
  alias Aether.Pipeline.Component.{Config, State}
  alias Aether.Pipeline.{Logger, Network, Stage}

  use GenServer, restart: :transient, shutdown: 10_000

  import Resolver.Process

  def start_link([config: config, pipeline_id: plid] = opts) do
    name = via_service_group(plid, config.path)

    GenServer.start_link(__MODULE__, opts, name: name)
  end

  def init(config: config, pipeline_id: plid) do
    service_group_set(plid)

    {:ok, %Config{config | pipeline_id: plid}, {:continue, :setup_pubsub}}
  end

  def stages(server),
    do: GenServer.call(server, :stages)

  def conn_opts(server),
    do: GenServer.call(server, :conn_opts)

  def connection_opts(server),
    do: GenServer.call(server, :conn_opts)

  def handle_continue(:setup_pubsub, %{name: name, type: type} = config) do
    with {:ok, pubsub} <- resolve_service(~p/pubsub/) do
      setup_subscriptions(pubsub, config)
      publish_initialized(pubsub, config)

      Logger.info("[#{type}] #{name} initialized")
    end

    {:noreply, config, {:continue, :init_stages}}
  end

  def handle_continue(:init_stages, %{path: path} = config) do
    with {:ok, supervisor} <- resolve_service(~p/#{path} stage supervisor/) do
      Stage.Supervisor.start_stages(supervisor, config, config.pipeline_id)
    end

    {:noreply, config}
  end

  def handle_call(:stages, _from, config) do
    with {:ok, stages} <- get_stage_pids(config) do
      {:reply, {:ok, stages}, config}
    end
  end

  def handle_call(:conn_opts, _from, config) do
    opts = Config.conn_opts(config)

    {:reply, {:ok, opts}, config}
  end

  def handle_info({:stage_init, %{payload: id}}, %{path: path} = config) do
    with {:ok, state} <- resolve_service(~p/#{path} state/) do
      State.stage_ready(state, id)
    end

    {:noreply, config}
  end

  def handle_info({:stage_subscribed, %{payload: {id, _, _}}}, %{path: path} = config) do
    with {:ok, state} <- resolve_service(~p/#{path} state/) do
      State.stage_active(state, id)
    end

    {:noreply, config}
  end

  def handle_info({:stage_done, %{payload: {id, _, _}}}, %{path: path} = config) do
    with {:ok, state} <- resolve_service(~p/#{path} state/) do
      State.stage_done(state, id)
    end

    {:noreply, config}
  end

  def handle_info({:stage_exit, _}, config), do: {:noreply, config}

  def handle_info({:stage_cancelled, _}, config) do
    unless any_subscriptions?(config), do: send_stages_done(config)

    {:noreply, config}
  end

  defp setup_subscriptions(pubsub, %{path: path}) do
    subscriptions = [
      {"#{path}:stage:init", {:send, :stage_init}},
      {"#{path}:stage:done", {:send, :stage_done}},
      {"#{path}:stage:exit", {:send, :stage_exit}},
      {"#{path}:stage:subscribed", {:send, :stage_subscribed}},
      {"#{path}:stage:cancelled", {:send, :stage_cancelled}}
    ]

    Enum.each(subscriptions, fn {topic, handler} ->
      PubSub.subscribe(pubsub, topic, handler)
    end)
  end

  defp publish_initialized(pubsub, config),
    do: PubSub.dispatch(pubsub, "component:init", config)

  defp get_stage_pid(path) do
    with {:ok, stage} <- resolve_service(path) do
      {path, stage}
    end
  end

  defp get_stage_pids(%{path: path}) do
    with {:ok, state} <- resolve_service(~p/#{path} state/) do
      stages =
        state
        |> State.stage_paths()
        |> Enum.map(&get_stage_pid/1)
        |> Enum.into(%{})

      {:ok, stages}
    end
  end

  defp any_subscriptions?(%{path: path}) do
    with {:ok, manager} <- resolve_service(~p/network manager/) do
      Network.any_subscriptions?(manager, path)
    else
      _ -> false
    end
  end

  defp send_stages_done(config) do
    with {:ok, stages} <- get_stage_pids(config) do
      Enum.each(stages, fn {_, pid} ->
        GenStage.async_info(pid, :done)
      end)
    end
  end
end
