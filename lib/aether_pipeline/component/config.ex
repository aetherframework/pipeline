defmodule Aether.Pipeline.Component.Config do
  @moduledoc """
  Component Config
  """

  alias GenStage.{BroadcastDispatcher, DemandDispatcher}

  @derive [Inspect]

  defstruct name: nil,
            path: nil,
            type: nil,
            module: nil,
            stage: nil,
            stages: 1,
            inputs: [],
            options: %{},
            channels: [:output],
            default_channel: nil,
            demand: %{min: nil, max: nil},
            selector: nil,
            dispatcher: nil,
            buffer_size: nil,
            pipeline_id: nil

  @default_channels [:output]
  @buffer_size 10_000
  @demand_min nil
  @demand_max nil
  @broadcast_dispatcher BroadcastDispatcher
  @demand_dispatcher DemandDispatcher

  def new(%{name: name, type: type, module: mod} = opts) do
    path = "#{type}/#{name}"

    inputs =
      opts
      |> Map.get(:inputs, [])
      |> Enum.map(&input_to_path/1)
      |> Enum.map(fn channel -> "#{channel}" end)

    demand =
      opts
      |> Map.get(:demand, %{})
      |> (&Map.merge(%{min: @demand_min, max: @demand_max}, &1)).()

    channels =
      case type do
        :sink ->
          []

        :channel ->
          []

        _ ->
          opts
          |> Map.get(:channels, @default_channels)
          |> build_channels()
      end

    %__MODULE__{
      name: name,
      path: path,
      type: type,
      module: mod,
      stage: Map.get(opts, :stage),
      inputs: inputs,
      options: Map.get(opts, :options, %{}),
      stages: Map.get(opts, :stages, 1),
      demand: demand,
      channels: channels,
      default_channel: get_default_channel(channels),
      selector: Map.get(opts, :selector, nil),
      dispatcher: select_dispatcher(opts),
      buffer_size: Map.get(opts, :buffer_size, @buffer_size)
    }
  end

  def conn_opts(%__MODULE__{demand: demand, selector: selector}) do
    opts = [
      cancel: :temporary,
      min_demand: demand.min,
      max_demand: demand.max,
      selector: selector
    ]

    Enum.filter(opts, fn {_, value} -> value end)
  end

  defp build_channels(channels) do
    channels
    |> Enum.map(&build_channel/1)
    |> Enum.uniq_by(&Keyword.get(&1, :name))
    |> Enum.map(&Map.new/1)
  end

  defp build_channel({name, opts}), do: Keyword.put(opts, :name, name)
  defp build_channel(channel), do: [name: channel]

  defp get_default_channel([]), do: nil

  defp get_default_channel(channels) do
    case Enum.find(channels, &Map.get(&1, :default, false)) do
      [name: name] -> name
      _ -> channels |> List.first() |> Map.get(:name)
    end
  end

  defp input_to_path(comp) when is_binary(comp), do: comp
  defp input_to_path({comp, channel}), do: "channel/#{comp}/#{channel}"

  defp select_dispatcher(%{dispatcher: :broadcast}), do: @broadcast_dispatcher
  defp select_dispatcher(%{dispatcher: :demand}), do: @demand_dispatcher
  defp select_dispatcher(%{type: :sink}), do: nil
  defp select_dispatcher(_), do: @broadcast_dispatcher
end
