defmodule Aether.Pipeline.Component.State do
  @moduledoc """
  Component State Machine
  """

  alias Aether.Core.{PubSub, StateMachine}
  alias Aether.Core.Service.Resolver
  alias Aether.Pipeline.{Logger, Network}

  use StateMachine, restart: :transient, shutdown: 10_000

  import Resolver.Process

  @derive [Inspect]

  defstruct name: nil,
            path: nil,
            type: nil,
            config: nil,
            stages: %{},
            pipeline_id: nil

  def start_link([config: config, pipeline_id: plid] = opts) do
    name = via_service_group(plid, ~p/#{config.path} state/)

    StateMachine.start_link(__MODULE__, opts, name: name)
  end

  def init(config: config, pipeline_id: plid) do
    service_group_set(plid)

    data = %__MODULE__{
      name: config.name,
      path: config.path,
      type: config.type,
      config: config,
      stages: init_stages_map(config),
      pipeline_id: plid
    }

    {:ok, :initializing, data}
  end

  def status(state),
    do: StateMachine.call(state, :status)

  def stage_ready(state, id),
    do: StateMachine.cast(state, {:stage_ready, id})

  def stage_active(state, id),
    do: StateMachine.cast(state, {:stage_active, id})

  def stage_done(state, id),
    do: StateMachine.cast(state, {:stage_done, id})

  def stage_paths(state),
    do: StateMachine.call(state, :stage_paths)

  def handle_cast({:stage_ready, id}, :initializing, data) do
    data = set_stage_ready(data, id)

    if all_stages_ready?(data) do
      {:noreply, {:next, :ready}, data, {:continue, :ready}}
    else
      {:noreply, :keep, data}
    end
  end

  def handle_cast({:stage_active, id}, :ready, data) do
    data = set_stage_active(data, id)

    if any_stages_active?(data) do
      {:noreply, {:next, :active}, data, {:continue, :active}}
    else
      {:noreply, :keep, data}
    end
  end

  def handle_cast({:stage_active, id}, :active, data) do
    data = set_stage_active(data, id)

    {:noreply, :keep, data}
  end

  def handle_cast({:stage_done, id}, :active, data) do
    data = set_stage_done(data, id)

    if all_stages_done?(data) do
      {:noreply, {:next, :done}, data, {:continue, :done}}
    else
      {:noreply, :keep, data}
    end
  end

  def handle_cast({:stage_done, id}, :done, data) do
    data = set_stage_done(data, id)

    {:noreply, :keep, data}
  end

  def handle_cast({:stage_done, id}, :complete, data) do
    data = set_stage_done(data, id)

    {:noreply, :keep, data}
  end

  def handle_call(:status, _from, state, _data),
    do: {:reply, state, :keep, :keep}

  def handle_call(:stage_paths, _from, _state, data) do
    paths =
      data.stages
      |> Map.keys()
      |> Enum.map(&stage_path(&1, data.path))

    {:reply, paths, :keep, :keep}
  end

  def handle_continue(:ready, :ready, data) do
    with {:ok, pubsub} <- resolve_service(~p/pubsub/) do
      PubSub.dispatch(pubsub, "component:ready", data.config)
      Logger.info("[#{data.type}] #{data.name} ready")
    end

    {:noreply, :keep, :keep}
  end

  def handle_continue(:active, :active, data) do
    with {:ok, pubsub} <- resolve_service(~p/pubsub/) do
      PubSub.dispatch(pubsub, "component:active", data.config)
      Logger.info("[#{data.type}] #{data.name} active")
    end

    {:noreply, :keep, :keep}
  end

  def handle_continue(:done, :done, data) do
    with {:ok, pubsub} <- resolve_service(~p/pubsub/),
         {:ok, manager} <- resolve_service(~p/network manager/) do
      %{config: config, name: name, path: path, type: type} = data

      Network.deactivate(manager, to: path, reason: {path, :done})
      PubSub.dispatch(pubsub, "component:done", config)
      Logger.info("[#{type}] #{name} done")
    end

    {:noreply, {:next, :complete}, data}
  end

  defp init_stages_map(%{stages: stages}) do
    0..(stages - 1)
    |> Enum.map(fn id -> %{id => :initializing} end)
    |> Enum.reduce(%{}, &Map.merge(&2, &1))
  end

  defp stage_path(id, path),
    do: ~p/#{path} stage #{id}/

  defp set_stage_ready(data, id),
    do: set_stage_status(data, id, :ready)

  defp set_stage_active(data, id),
    do: set_stage_status(data, id, :active)

  defp set_stage_done(data, id),
    do: set_stage_status(data, id, :done)

  defp set_stage_status(%{stages: stages} = data, id, status),
    do: %{data | stages: Map.put(stages, id, status)}

  defp all_stages_have_status?(%{stages: stages}, status),
    do: stages |> Map.values() |> Enum.all?(&(&1 == status))

  defp any_stages_have_status?(%{stages: stages}, status),
    do: stages |> Map.values() |> Enum.any?(&(&1 == status))

  defp all_stages_ready?(data),
    do: all_stages_have_status?(data, :ready)

  defp any_stages_active?(data),
    do: any_stages_have_status?(data, :active)

  defp all_stages_done?(data),
    do: all_stages_have_status?(data, :done)
end
