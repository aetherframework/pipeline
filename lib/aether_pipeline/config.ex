defmodule Aether.Pipeline.Config do
  @moduledoc """
  Pipeline Config
  """

  @derive [Inspect]

  @default_logger_config %{level: :info}

  defstruct [
    :logger
  ]

  def new(opts) do
    %__MODULE__{logger: logger_config(opts)}
  end

  defp logger_config(%{logger: logger}),
    do: Map.merge(@default_logger_config, logger)

  defp logger_config(_), do: @default_logger_config
end
