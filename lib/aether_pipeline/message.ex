defmodule Aether.Pipeline.Message do
  @moduledoc """
  Message module
  """

  alias Aether.Pipeline.Message.Batch

  defstruct data: %{}, meta: %{}, errors: []

  @type t :: %__MODULE__{
          data: map(),
          meta: map(),
          errors: [{String.t(), atom()}]
        }

  @default_meta %{history: []}

  @doc """
  Creates a new Message struct

  ## Examples

      iex> Aether.Pipeline.Message.new(%{foo: :bar})
      #Aether.Pipeline.Message<data: %{foo: :bar}, meta: %{history: []}, errors: []>

      iex> Aether.Pipeline.Message.new(data: %{foo: :bar})
      #Aether.Pipeline.Message<data: %{foo: :bar}, meta: %{history: []}, errors: []>

      iex> Aether.Pipeline.Message.new(%{data: %{foo: :bar}})
      #Aether.Pipeline.Message<data: %{foo: :bar}, meta: %{history: []}, errors: []>

  """
  def new(%{data: data}), do: %__MODULE__{data: data, meta: @default_meta}
  def new(data: data), do: %__MODULE__{data: data, meta: @default_meta}
  def new(data), do: %__MODULE__{data: data, meta: @default_meta}

  @doc """
  Gets a data value for the given key from the message. Returns `nil`
  if the key doesn't exist in the data map.

  ## Examples

      iex> msg = Aether.Pipeline.Message.new(%{foo: :bar})
      iex> Aether.Pipeline.Message.get_data(msg, :foo)
      :bar

      iex> msg = Aether.Pipeline.Message.new(%{foo: :bar})
      iex> Aether.Pipeline.Message.get_data(msg, :baz)
      nil

  """
  def get_data(%__MODULE__{data: data}, key),
    do: Map.get(data, key, nil)

  @doc """
  Puts a new value for the given key in the messages data map.

  ## Examples

      iex> msg = Aether.Pipeline.Message.new(%{foo: :bar})
      iex> Aether.Pipeline.Message.put_data(msg, :bar, :baz)
      #Aether.Pipeline.Message<data: %{bar: :baz, foo: :bar}, meta: %{history: []}, errors: []>

      iex> msg = Aether.Pipeline.Message.new(data: %{foo: :bar})
      iex> Aether.Pipeline.Message.put_data(msg, :foo, :baz)
      #Aether.Pipeline.Message<data: %{foo: :baz}, meta: %{history: []}, errors: []>

  """
  def put_data(%__MODULE__{data: data} = msg, key, value),
    do: %__MODULE__{msg | data: Map.put(data, key, value)}

  @doc """
  Removes the passed key(s) from the messages data map.

  ## Examples

      iex> msg = Aether.Pipeline.Message.new(%{foo: :bar, bar: :baz})
      iex> Aether.Pipeline.Message.drop_data(msg, :bar)
      #Aether.Pipeline.Message<data: %{foo: :bar}, meta: %{history: []}, errors: []>

      iex> msg = Aether.Pipeline.Message.new(%{foo: :bar, bar: :baz})
      iex> Aether.Pipeline.Message.drop_data(msg, [:foo, :bar])
      #Aether.Pipeline.Message<data: %{}, meta: %{history: []}, errors: []>

  """
  def drop_data(%__MODULE__{} = msg, key) when not is_list(key),
    do: drop_data(msg, [key])

  def drop_data(%__MODULE__{data: data} = msg, keys),
    do: %__MODULE__{msg | data: Map.drop(data, keys)}

  @doc """
  Merges the passed map with the messages data map.

  ## Examples

      iex> msg = Aether.Pipeline.Message.new(%{foo: :bar})
      iex> Aether.Pipeline.Message.merge_data(msg, %{bar: :baz})
      #Aether.Pipeline.Message<data: %{bar: :baz, foo: :bar}, meta: %{history: []}, errors: []>

      iex> msg = Aether.Pipeline.Message.new(%{foo: :bar})
      iex> Aether.Pipeline.Message.merge_data(msg, %{foo: :qux, bar: :baz})
      #Aether.Pipeline.Message<data: %{bar: :baz, foo: :qux}, meta: %{history: []}, errors: []>

  """
  def merge_data(%__MODULE__{data: data} = msg, updates),
    do: %__MODULE__{msg | data: Map.merge(data, updates)}

  def get_meta(%__MODULE__{meta: meta}, key),
    do: Map.get(meta, key, nil)

  def put_meta(%__MODULE__{meta: meta} = msg, key, value),
    do: %__MODULE__{msg | meta: Map.put(meta, key, value)}

  def drop_meta(%__MODULE__{} = msg, key) when not is_list(key),
    do: drop_meta(msg, [key])

  def drop_meta(%__MODULE__{meta: meta} = msg, keys),
    do: %__MODULE__{msg | meta: Map.drop(meta, keys)}

  def update_meta(%__MODULE__{meta: meta} = msg, updates),
    do: %__MODULE__{msg | meta: Map.merge(meta, updates)}

  def put_error(%__MODULE__{errors: errors} = msg, error),
    do: %__MODULE__{msg | errors: [error | errors]}

  def set_from(%__MODULE__{} = msg, from),
    do: put_meta(msg, :from, from)

  def put_history(%__MODULE__{meta: %{history: hist}} = msg, path),
    do: put_meta(msg, :history, [path | hist])

  def put_channel(%__MODULE__{} = msg, channel),
    do: put_meta(msg, :channel, channel)

  def drop_channel(%__MODULE__{} = msg),
    do: drop_meta(msg, :channel)

  def set_stage_meta(%__MODULE__{} = msg, %{config: config}) do
    msg
    |> set_from(config.path)
    |> put_history(config.path)
    |> set_channel(config)
  end

  def set_stage_meta(%Batch{} = batch, stage),
    do: Batch.set_stage_meta(batch, stage)

  defp set_channel(%__MODULE__{} = msg, %{type: :channel}),
    do: drop_channel(msg)

  defp set_channel(%__MODULE__{} = msg, %{options: %{router: router}} = config) do
    msg
    |> select_channel(router, config)
    |> (&put_channel(msg, &1)).()
  end

  defp set_channel(%__MODULE__{} = msg, %{default_channel: channel}),
    do: put_channel(msg, channel)

  defp select_channel(channel, _) when not is_nil(channel), do: channel
  defp select_channel(_, %{default_channel: channel}), do: channel

  defp select_channel(msg, router, config) when is_function(router, 1),
    do: msg |> router.() |> select_channel(config)

  defp select_channel(msg, router, config) when is_function(router, 2),
    do: msg |> router.(config) |> select_channel(config)

  defimpl Inspect do
    import Inspect.Algebra

    def inspect(%{data: data, meta: meta, errors: errors}, opts) do
      concat([
        "#Aether.Pipeline.Message<",
        "data: ",
        to_doc(data, opts),
        ", ",
        "meta: ",
        to_doc(meta, opts),
        ", ",
        "errors: #{inspect(errors)}",
        ">"
      ])
    end
  end
end
