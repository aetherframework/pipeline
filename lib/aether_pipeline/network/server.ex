defmodule Aether.Pipeline.Network.Server do
  @moduledoc """
  Network server
  """

  alias Aether.Core.PubSub
  alias Aether.Core.Service.Resolver
  alias Aether.Pipeline.Logger
  alias Aether.Pipeline.Network.Manager

  use GenServer, restart: :transient, shutdown: 10_000

  import Resolver.Process

  def start_link([spec: _, pipeline_id: plid] = opts) do
    name = via_service_group(plid, ~p/network manager/)

    GenServer.start_link(__MODULE__, opts, name: name)
  end

  def init(spec: spec, pipeline_id: plid) do
    service_group_set(plid)
    Process.flag(:trap_exit, true)

    manager = Manager.new(spec: spec, pipeline_id: plid)

    {:ok, manager, {:continue, :setup_pubsub}}
  end

  def state(server), do: GenServer.call(server, :state)

  def any_subscriptions?(server, path),
    do: GenServer.call(server, {:any_subscriptions?, path})

  def any_subscribers?(server, path),
    do: GenServer.call(server, {:any_subscribers?, path})

  def active_links?(server, path),
    do: GenServer.call(server, {:active_links?, path})

  def deactivate(server, opts),
    do: GenServer.cast(server, {:deactivate, opts})

  def handle_continue(:setup_pubsub, manager) do
    with {:ok, pubsub} <- resolve_service(~p/pubsub/) do
      setup_subscriptions(pubsub)
      publish_initialized(pubsub)

      Logger.info("[pipeline] network initialized")
    end

    {:noreply, manager}
  end

  def handle_call(:state, _from, manager),
    do: {:reply, manager, manager}

  def handle_call({:any_subscriptions?, path}, _from, manager),
    do: {:reply, Manager.any_subscriptions?(manager, path), manager}

  def handle_call({:any_subscribers?, path}, _from, manager),
    do: {:reply, Manager.any_subscribers?(manager, path), manager}

  def handle_call({:active_links?, path}, _from, manager),
    do: {:reply, Manager.active_links?(manager, path), manager}

  def handle_cast({:deactivate, opts}, manager),
    do: {:noreply, Manager.deactivate(manager, opts)}

  def handle_info({:pipeline_ready, _}, manager) do
    Logger.info("[pipeline] linking stages")

    manager = Manager.activate(manager)

    {:noreply, manager}
  end

  def handle_info({:DOWN, ref, :process, _pid, reason}, manager) do
    manager = Manager.deactivate(manager, ref: ref, reason: {:DOWN, reason})

    {:noreply, manager}
  end

  defp setup_subscriptions(pubsub) do
    subscriptions = [
      {"pipeline:ready", {:send, :pipeline_ready}}
    ]

    Enum.each(subscriptions, fn {topic, handler} ->
      PubSub.subscribe(pubsub, topic, handler)
    end)
  end

  defp publish_initialized(pubsub),
    do: PubSub.dispatch(pubsub, "network:manager:init", %{})
end
