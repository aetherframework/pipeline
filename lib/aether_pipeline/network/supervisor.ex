defmodule Aether.Pipeline.Network.Supervisor do
  @moduledoc """
  Pipeline Network Supervisor
  """

  alias Aether.Core.Service.Resolver
  alias Aether.Pipeline.Network

  use Supervisor

  import Resolver.Process

  def start_link([spec: _, pipeline_id: plid] = opts) do
    name = via_service_group(plid, ~p/network supervisor/)

    Supervisor.start_link(__MODULE__, opts, name: name)
  end

  def init(spec: spec, pipeline_id: plid) do
    service_group_set(plid)

    children = [
      {Network.Server, spec: spec, pipeline_id: plid}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
