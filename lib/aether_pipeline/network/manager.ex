defmodule Aether.Pipeline.Network.Manager do
  @moduledoc """
  Pipeline network manager
  """

  alias Aether.Core.Service
  alias Aether.Pipeline
  alias Aether.Pipeline.{Component, Graph, Spec, Stage}
  alias Aether.Pipeline.Network.Link

  import Service.Resolver.Process

  @derive {Inspect, only: [:links]}

  defstruct refs: %{}, links: [], pipeline_id: nil

  @type ref_value ::
          {Component.path(), pid()}
          | {Component.path(), Stage.path(), pid()}

  @type t :: %__MODULE__{
          refs: %{reference() => ref_value()},
          links: [Link.t()],
          pipeline_id: Pipeline.id()
        }

  def new(spec: %Spec{graph: graph}, pipeline_id: plid) do
    links =
      graph
      |> Graph.component_links()
      |> Enum.map(&build_link(&1, plid))

    %__MODULE__{links: links, pipeline_id: plid}
  end

  def activate(%__MODULE__{refs: refs, links: links} = manager, opts \\ []) do
    opts =
      opts
      |> Keyword.take([:path, :from, :to])
      |> Keyword.merge(status: :inactive)

    {links, refs} =
      links
      |> filter_links(opts)
      |> activate_links(refs)

    %__MODULE__{manager | refs: refs, links: links}
  end

  def deactivate(manager, ref: ref, reason: reason) when is_reference(ref),
    do: deactivate_ref(manager, ref, reason)

  def deactivate(manager, ref: ref) when is_reference(ref),
    do: deactivate_ref(manager, ref)

  def deactivate(manager, to: path, reason: reason) when is_binary(path),
    do: deactivate_path(manager, to: path, reason: reason)

  def deactivate(manager, from: path, reason: reason) when is_binary(path),
    do: deactivate_path(manager, from: path, reason: reason)

  def any_subscriptions?(%__MODULE__{} = manager, path),
    do: manager |> active_subscriptions(path) |> length() > 0

  def any_subscribers?(%__MODULE__{} = manager, path),
    do: manager |> active_subscribers(path) |> length() > 0

  def active_links?(%__MODULE__{} = manager, path),
    do: any_subscriptions?(manager, path) or any_subscribers?(manager, path)

  defp build_link({from, to}, plid),
    do: Link.new(to: to, from: from, pipeline_id: plid)

  defp activate_links(links, refs) do
    Enum.map_reduce(links, refs, fn link, acc ->
      {link, refs} = activate_link(link, refs)

      {link, Map.merge(acc, refs)}
    end)
  end

  defp activate_link(link, refs) do
    with {:ok, to} <- resolve_service(link.to),
         {:ok, from} <- resolve_service(link.from),
         {:ok, producers} <- Component.stages(to),
         {:ok, consumers} <- Component.stages(from),
         {:ok, conn_opts} <- Component.connection_opts(from) do
      {:ok, link} =
        Link.activate(
          link,
          producers,
          consumers,
          conn_opts
        )

      {:ok, refs} =
        monitor_link(
          refs,
          link,
          {to, producers},
          {from, consumers}
        )

      {link, refs}
    end
  end

  defp monitor_link(refs, link, {to, producers}, {from, consumers}) do
    refs =
      refs
      |> monitor_component(link.to, to, producers)
      |> monitor_component(link.from, from, consumers)

    {:ok, refs}
  end

  defp monitor_component(refs, path, pid, stages) do
    ref = Process.monitor(pid)

    refs
    |> Map.put_new(ref, {path, pid})
    |> monitor_stages(stages, path)
  end

  defp monitor_stages(refs, stages, component) do
    Enum.reduce(stages, refs, fn {path, pid}, refs ->
      ref = Process.monitor(pid)

      Map.put_new(refs, ref, {component, path, pid})
    end)
  end

  defp lookup_ref(%{refs: refs}, ref) do
    case Map.get(refs, ref) do
      {path, _} -> {:ok, :component, path}
      {comp, path, _} -> {:ok, :stage, {path, comp}}
      _ -> {:error, :no_path_for_ref, ref}
    end
  end

  defp deactivate_ref(manager, ref, reason \\ nil) do
    case lookup_ref(manager, ref) do
      {:ok, :component, path} ->
        manager
        |> deactivate_path(to: path, reason: reason)
        |> deactivate_path(from: path, reason: reason)

      {:ok, :stage, stage} ->
        deactivate_path(manager, stage: stage, reason: reason)

      {:error, _, _} ->
        manager
    end
  end

  defp deactivate_path(manager, to: path, reason: reason),
    do: deactivate_path(manager, :to, path, reason)

  defp deactivate_path(manager, from: path, reason: reason),
    do: deactivate_path(manager, :from, path, reason)

  defp deactivate_path(manager, stage: stage, reason: reason),
    do: deactivate_path(manager, :stage, stage, reason)

  defp deactivate_path(manager, :stage, {path, component}, _) do
    %{refs: refs, links: links} = manager

    links =
      Enum.map(links, fn
        %{stages: %{^path => _}} = link ->
          {:ok, link} = Link.drop_stage(link, path)
          link

        link ->
          link
      end)

    refs =
      Enum.reject(refs, fn
        {ref, {^component, ^path, _}} -> Process.demonitor(ref)
        _ -> false
      end)

    %__MODULE__{manager | refs: refs, links: links}
  end

  defp deactivate_path(manager, direction, path, reason) do
    %{refs: refs, links: links} = manager

    links =
      Enum.map(links, fn
        %{^direction => ^path} = link ->
          {:ok, link} = Link.deactivate(link, reason)
          link

        link ->
          link
      end)

    refs =
      Enum.reject(refs, fn
        {ref, {^path, _}} -> Process.demonitor(ref)
        {ref, {^path, _, _}} -> Process.demonitor(ref)
        _ -> false
      end)

    %__MODULE__{manager | refs: refs, links: links}
  end

  defp active_subscriptions(%__MODULE__{links: links}, path) do
    Enum.filter(links, fn
      %{status: :active, from: ^path} -> true
      _ -> false
    end)
  end

  defp active_subscribers(%__MODULE__{links: links}, path) do
    Enum.filter(links, fn
      %{status: :active, to: ^path} -> true
      _ -> false
    end)
  end

  defp filter_links(links, status: status, path: path) do
    Enum.filter(links, fn
      %{status: ^status, to: ^path} -> true
      %{status: ^status, from: ^path} -> true
      _ -> false
    end)
  end

  defp filter_links(links, status: status, to: path) do
    Enum.filter(links, fn
      %{status: ^status, to: ^path} -> true
      _ -> false
    end)
  end

  defp filter_links(links, status: status, from: path) do
    Enum.filter(links, fn
      %{status: ^status, from: ^path} -> true
      _ -> false
    end)
  end

  defp filter_links(links, status: status, stage: path) do
    Enum.filter(links, fn
      %{status: ^status, stages: %{^path => _}} -> true
      _ -> false
    end)
  end

  defp filter_links(links, status: status) do
    Enum.filter(links, fn
      %{status: ^status} -> true
      _ -> false
    end)
  end

  defp filter_links(links, path: path) do
    Enum.filter(links, fn
      %{to: ^path} -> true
      %{from: ^path} -> true
      _ -> false
    end)
  end

  defp filter_links(links, to: path) do
    Enum.filter(links, fn
      %{to: ^path} -> true
      _ -> false
    end)
  end

  defp filter_links(links, from: path) do
    Enum.filter(links, fn
      %{from: ^path} -> true
      _ -> false
    end)
  end

  defp filter_links(links, stage: path) do
    Enum.filter(links, fn
      %{stages: %{^path => _}} -> true
      _ -> false
    end)
  end
end
