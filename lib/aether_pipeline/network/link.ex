defmodule Aether.Pipeline.Network.Link do
  @moduledoc """
  Network link struct
  """

  alias Aether.Pipeline
  alias Aether.Pipeline.{Component, Stage}

  @derive {Inspect, only: [:to, :from, :status]}

  defstruct to: nil,
            from: nil,
            stages: %{},
            status: :inactive,
            pipeline_id: nil

  @type status :: :inactive | :active
  @type stages :: %{Stage.path() => Stage.link_ref()}

  @type t :: %__MODULE__{
          to: Component.path(),
          from: Component.path(),
          status: status(),
          stages: stages(),
          pipeline_id: Pipeline.id()
        }

  def new(to: to, from: from, pipeline_id: plid),
    do: %__MODULE__{to: to, from: from, pipeline_id: plid}

  def activate(%__MODULE__{from: from} = link, producers, consumers, opts) do
    opts = [{:path, from} | opts]

    with {:ok, stages} <- link_stages(producers, consumers, opts) do
      link =
        link
        |> set_stages(stages)
        |> set_status(:active)

      {:ok, link}
    else
      {:error, reason, _} -> {:error, reason}
    end
  end

  def deactivate(%__MODULE__{stages: stages} = link, reason) do
    with :ok <- unlink_stages(stages, reason) do
      link =
        link
        |> set_stages(%{})
        |> set_status(:inactive)

      {:ok, link}
    else
      _ -> {:error, link}
    end
  end

  def drop_stage(link, path, reason \\ nil)

  def drop_stage(%__MODULE__{} = link, path, reason) when is_binary(path),
    do: drop_stages(link, [path], reason)

  def drop_stage(%__MODULE__{} = link, paths, reason) when is_list(paths),
    do: drop_stages(link, paths, reason)

  def set_status(link, status),
    do: %__MODULE__{link | status: status}

  def set_stages(link, stages),
    do: %__MODULE__{link | stages: stages}

  defp link_stages(producers, consumers, opts) do
    stages =
      for consumer <- consumers, producer <- producers do
        with {:ok, path, tag} <- Stage.link(consumer, producer, opts) do
          {path, tag}
        else
          {:error, {:bad_opts, msg}} -> {:error, :bad_opts, msg}
          {:error, reason} -> {:error, reason, {consumer, producer, opts}}
        end
      end

    if Enum.any?(stages, &match?({:error, _, _}, &1)) do
      Enum.find(stages, &match?({:error, _, _}, &1))
    else
      {:ok, Enum.into(stages, %{})}
    end
  end

  defp unlink_stages(stages, reason) when is_map(stages),
    do: stages |> Map.values() |> unlink_stages(reason)

  defp unlink_stages(stages, reason) when is_list(stages) do
    errors =
      stages
      |> Enum.map(&Stage.unlink(&1, reason))
      |> Enum.any?(fn val -> val != :ok end)

    if errors, do: :error, else: :ok
  end

  defp drop_stages(%{status: status, stages: stages} = link, paths, reason) do
    {dropped, stages} = Map.split(stages, paths)

    with :ok <- unlink_stages(dropped, reason) do
      link =
        case {status, Enum.empty?(stages)} do
          {:active, true} ->
            link |> set_stages(stages) |> set_status(:inactive)

          {:active, false} ->
            link |> set_stages(stages)

          {:inactive, true} ->
            link |> set_stages(stages)

          {:inactive, false} ->
            link |> set_stages(stages) |> set_status(:active)
        end

      {:ok, link}
    else
      _ -> {:error, link}
    end
  end
end
