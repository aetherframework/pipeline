defmodule Aether.Pipeline.Output do
  @moduledoc """
  Pipeline output
  """

  alias Aether.Pipeline.Output.Server

  defdelegate get(pid, key), to: Server
  defdelegate get(pid, key, default), to: Server
  defdelegate put(pid, key, value), to: Server
  defdelegate put_new(pid, key, value), to: Server
  defdelegate update(pid, key, fun), to: Server
  defdelegate update(pid, key, default, fun), to: Server
end
