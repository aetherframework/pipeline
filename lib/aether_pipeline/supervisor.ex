defmodule Aether.Pipeline.Supervisor do
  @moduledoc """
  Pipeline supervisor
  """

  alias Aether.Core.PubSub
  alias Aether.Core.Service.{Group, Registrar, Resolver}
  alias Aether.Pipeline.{Executor, Logger, Network, Output}

  use Supervisor

  import Resolver.Process

  def start_link([spec: _, pipeline_id: plid, owner: _] = opts) do
    with {:ok, pid} <- Supervisor.start_link(__MODULE__, opts) do
      plid
      |> Registrar.lookup()
      |> Group.register(~p/pipeline supervisor/, pid)

      {:ok, pid}
    else
      value -> value
    end
  end

  def init(spec: spec, pipeline_id: plid, owner: owner) do
    service_group_set(plid)

    children = [
      {Group.Supervisor, group: plid},
      {PubSub.Supervisor, group: plid, dispatcher: :async},
      {Logger.Supervisor, spec: spec, pipeline_id: plid},
      {Output.Supervisor, spec: spec, pipeline_id: plid},
      {Network.Supervisor, spec: spec, pipeline_id: plid},
      {Executor.Supervisor, spec: spec, pipeline_id: plid, owner: owner}
    ]

    Supervisor.init(children, strategy: :rest_for_one)
  end
end
