defmodule Aether.Pipeline.Sink do
  @moduledoc """
  This is the Output module
  """

  @callback init(config :: term()) :: term()
  @callback call(message :: term(), state :: term(), config :: term()) :: term()
  @callback flush(state :: term(), config :: term()) :: {:ok, [term()], term()}

  defmacro __using__(opts) do
    quote location: :keep, bind_quoted: [opts: opts, module: __CALLER__.module] do
      alias Aether.Pipeline.{Component, Sink, Stage}

      use Component, type: :sink, stage: Stage.Consumer

      @behaviour Sink

      @impl Sink
      def init(_config), do: {:ok, %{}}

      @impl Sink
      def call(_message, state, _config), do: {:ok, state}

      @impl Sink
      def flush(state, _config), do: {:ok, [], state}

      def consume_message(message, state, config),
        do: call(message, state, config)

      defoverridable init: 1,
                     call: 3,
                     flush: 2
    end
  end
end
