defmodule Aether.Pipeline.Source.List do
  @moduledoc """
  This is the Source List module
  """

  alias Aether.Core.Utils
  alias Aether.Pipeline.Source

  use Source

  @impl Source
  def init(%{options: options}) do
    stream = init_stream(options)

    {:ok, %{stream: stream}}
  end

  def call(count, state, config)
  def call(_, %{stream: :done} = state, _), do: {:ok, [], state}

  def call(count, %{stream: stream} = state, _config) do
    case take_from_stream(stream, count) do
      {:ok, items, :done} ->
        {:done, items, %{state | stream: :done}}

      {:ok, :done, _} ->
        {:ok, [], %{state | stream: :done}}

      {:ok, items, stream} ->
        {:ok, items, %{state | stream: stream}}

      {:error, _error, _} ->
        {:error, [], state}

      _ ->
        {:error, [], state}
    end
  end

  def options do
    %{
      data: [type: :list, default: []],
      cycle: [type: :bool, default: false],
      max: [type: :int]
    }
  end

  defp init_stream(%{data: data, max: max, cycle: true}),
    do: init_stream(%{data: Stream.cycle(data), max: max})

  defp init_stream(%{data: data, max: max}) when is_integer(max),
    do: Stream.take(data, max)

  defp init_stream(%{data: data}), do: data

  defp take_from_stream(stream, count) do
    case Utils.Stream.take(stream, count) do
      {:ok, {[], _, _}} -> {:ok, :done, :done}
      {:ok, {items, _, 0}} -> {:ok, items, :done}
      {:ok, {items, updated, _}} -> {:ok, items, updated}
      {:error, error} -> {:error, error, stream}
    end
  end
end
