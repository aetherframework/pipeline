defmodule Aether.Pipeline.Source.CSV do
  @moduledoc """
  This is the Source CSV module
  """

  alias Aether.Core.Utils
  alias Aether.Pipeline.Source

  use Source

  def init(%{options: %{file: file, headers: headers}}) do
    {:ok, stream} = Utils.CSV.stream(file, headers: headers)

    {:ok, %{stream: stream}}
  end

  def init(%{options: %{file: file}}) do
    {:ok, stream} = Utils.CSV.stream(file)

    {:ok, %{stream: stream}}
  end

  def call(count, state, config)
  def call(_, %{stream: :done} = state, _), do: {:ok, [], state}

  def call(count, %{stream: stream} = state, _) do
    case take_from_stream(stream, count) do
      {:ok, items, :done} ->
        {:done, items, %{state | stream: :done}}

      {:ok, :done, _} ->
        {:ok, [], %{state | stream: :done}}

      {:ok, items, stream} ->
        {:ok, items, %{state | stream: stream}}

      {:error, _error, _} ->
        {:error, [], state}

      _ ->
        {:error, [], state}
    end
  end

  def options do
    %{
      file: [type: :binary, required: true],
      headers: [type: :list, required: true]
    }
  end

  defp take_from_stream(stream, count) do
    case Utils.Stream.take(stream, count) do
      {:ok, {[], _, _}} -> {:ok, :done, :done}
      {:ok, {items, _, 0}} -> {:ok, items, :done}
      {:ok, {items, updated, _}} -> {:ok, items, updated}
      {:error, error} -> {:error, error, stream}
    end
  end
end
