defmodule Aether.Pipeline.Channel do
  @moduledoc """
  This is the Channel module
  """

  alias Aether.Pipeline.{Component, Stage}

  use Component, type: :channel, stage: Stage.Processor

  def init(_), do: {:ok, %{}}

  def flush(state, _config), do: {:ok, [], state}

  def process_message(message, state, _config),
    do: {:ok, message, state}
end
