defmodule Aether.Pipeline.Stage do
  @moduledoc """
  This is the Stage module
  """

  alias __MODULE__
  alias Aether.Core.Service
  alias Aether.Pipeline
  alias Aether.Pipeline.Message
  alias Aether.Pipeline.Stage.Controller

  import Service.Resolver.Process

  defstruct [
    :id,
    :path,
    :type,
    :config,
    :state,
    :pipeline_id
  ]

  @opaque path :: Service.path()
  @opaque type :: GenStage.type()

  @type link_ref :: {pid(), GenStage.subscription_tag()}
  @type messages :: [Message.t()] | [Message.Batch.t()] | [Message.Control.t()]

  @type start_link_opts :: [
          {:id, integer()},
          {:config, term()},
          {:pipeline_id, Pipeline.id()}
        ]

  @type stage_init_tuple :: {
          integer(),
          path(),
          type(),
          term(),
          Pipeline.id()
        }

  @type link_response ::
          {:ok, path(), link_ref()}
          | {:error, :bad_opts, term()}
          | {:error, :not_a_consumer | atom(), {pid, pid, keyword()}}

  @type t :: %__MODULE__{
          id: integer(),
          path: path(),
          type: type(),
          config: term(),
          state: map(),
          pipeline_id: Pipeline.id()
        }

  @typep error_response_reason :: String.t() | atom()

  @type producer_response ::
          {:ok, messages(), Stage.t()}
          | {:error, messages(), Stage.t()}
          | {:error, Stage.t()}
          | {:error, error_response_reason(), Stage.t()}

  @type processor_response :: producer_response()

  @type consumer_response ::
          {:ok, Stage.t()}
          | {:error, Stage.t()}
          | {:error, error_response_reason(), Stage.t()}

  @callback produce_messages(count :: non_neg_integer(), stage :: Stage.t()) ::
              producer_response()
  @callback process_messages(messages :: messages(), stage :: Stage.t()) ::
              processor_response()
  @callback consume_messages(messages :: messages(), stage :: Stage.t()) ::
              consumer_response()

  @optional_callbacks produce_messages: 2,
                      process_messages: 2,
                      consume_messages: 2

  @spec __using__(opts :: [{:type, type()}]) :: term()
  defmacro __using__(type: type) do
    quote location: :keep, bind_quoted: [type: type, module: __CALLER__.module] do
      alias Aether.Core.Service.Resolver
      alias Aether.Pipeline.{Message, Stage}
      alias Aether.Pipeline.Stage.Controller

      use GenStage, restart: :transient, shutdown: 10_000

      @behaviour Stage

      import Resolver.Process

      def start_link(opts),
        do: Stage.start_link(unquote(module), opts)

      def init(id: id, path: path, config: config, pipeline_id: plid),
        do: Stage.init({id, path, unquote(type), config, plid})

      def produce_messages(_count, stage), do: {:ok, [], stage}
      def process_messages(messages, stage), do: {:ok, messages, stage}
      def consume_messages(_messages, stage), do: {:ok, stage}

      def handle_demand(count, %{type: :producer} = stage) do
        with {:ok, messages, stage} <- produce_messages(count, stage) do
          messages = Enum.map(messages, &Message.set_stage_meta(&1, stage))

          {:noreply, messages, stage}
        else
          _ -> {:noreply, [], stage}
        end
      end

      def handle_events(messages, _from, %{type: :producer_consumer} = stage) do
        with {:ok, messages, stage} <- process_messages(messages, stage) do
          messages = Enum.map(messages, &Message.set_stage_meta(&1, stage))

          {:noreply, messages, stage}
        else
          _ -> {:noreply, [], stage}
        end
      end

      def handle_events(messages, _from, %{type: :consumer} = stage) do
        with {:ok, stage} <- consume_messages(messages, stage) do
          {:noreply, [], stage}
        else
          _ -> {:noreply, [], stage}
        end
      end

      def handle_subscribe(_type, opts, _from, stage),
        do: Controller.subscribed(opts, stage)

      def handle_cancel({:cancel, reason}, _from, stage),
        do: Controller.cancelled(reason, stage)

      def handle_cancel({:down, :shutdown}, _from, stage),
        do: {:noreply, [], stage}

      def handle_info(:flush, stage),
        do: Controller.flush(stage)

      def handle_info(:done, stage),
        do: Controller.done(stage)

      def handle_control_message(%Message.Control{} = message, stage),
        do: Controller.control_message(message, stage)

      defoverridable produce_messages: 2,
                     consume_messages: 2,
                     process_messages: 2
    end
  end

  def start_link(mod, id: id, config: config, pipeline_id: plid) do
    path = ~p/#{config.path} stage #{id}/
    name = via_service_group(plid, path)
    opts = [id: id, path: path, config: config, pipeline_id: plid]

    with {:ok, pid} <- GenStage.start_link(mod, opts, name: name) do
      Controller.initialized(id, config, plid)

      {:ok, pid}
    else
      val -> val
    end
  end

  def init({id, path, type, config, plid}) do
    service_group_set(plid)

    with {:ok, state} <- config.module.init(config) do
      stage = %__MODULE__{
        id: id,
        path: path,
        type: type,
        config: config,
        state: state,
        pipeline_id: plid
      }

      {type, stage, stage_opts(config, type)}
    else
      _ -> {:stop, :could_not_init_state}
    end
  end

  def link({cons_path, cons_pid} = cons, {prod_path, prod_pid} = prod, opts \\ []) do
    opts = Keyword.merge([to: prod_pid, stage: cons_path], opts)

    with {:ok, tag} <- GenStage.sync_subscribe(cons_pid, opts) do
      {:ok, prod_path, {prod_pid, tag}}
    else
      _ -> {:error, :could_not_link_stages, {cons, prod, opts}}
    end
  end

  def unlink({_prod_pid, _tag} = subscription, reason),
    do: GenStage.cancel(subscription, reason)

  defp stage_opts(_config, :consumer), do: []

  defp stage_opts(config, :producer),
    do: [dispatcher: config.dispatcher, buffer_size: config.buffer_size]

  defp stage_opts(config, :producer_consumer),
    do: [dispatcher: config.dispatcher, buffer_size: config.buffer_size]
end
