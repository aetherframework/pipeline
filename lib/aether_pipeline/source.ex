defmodule Aether.Pipeline.Source do
  @moduledoc """
  This is the Source module
  """

  @callback init(config :: term()) :: term()
  @callback call(message :: term(), state :: term(), config :: term()) :: term()
  @callback flush(state :: term(), config :: term()) :: {[term()], term()}

  defmacro __using__(_opts) do
    quote location: :keep do
      alias Aether.Pipeline.{Component, Source, Stage}

      use Component, type: :source, stage: Stage.Producer

      @behaviour Source

      @impl Source
      def init(_config), do: {:ok, %{}}

      @impl Source
      def call(_count, state, _config), do: {:ok, [], state}

      @impl Source
      def flush(state, _config), do: {:ok, [], state}

      def produce_messages(count, state, config),
        do: call(count, state, config)

      defoverridable init: 1,
                     call: 3,
                     flush: 2
    end
  end
end
