defmodule Aether.Pipeline.Sink.Logger do
  @moduledoc """
  Logger output module
  """

  alias Aether.Pipeline.{Logger, Sink}

  use Sink

  @default_level :info

  def init(_config), do: {:ok, %{logger: Logger}}

  def call(message, state, config) do
    :ok = log(message, state, config)

    {:ok, state}
  end

  def options do
    %{
      level: [type: :atom, default: :info],
      formatter: [type: :function]
    }
  end

  defp log(message, state, %{options: %{level: level}} = config),
    do: log(level, message, state, config)

  defp log(message, state, config),
    do: log(@default_level, message, state, config)

  defp log(level, message, %{logger: logger}, config) do
    message
    |> format(config)
    |> inspect()
    |> (&logger.log(level, &1, [])).()

    :ok
  end

  defp format(message, %{options: %{formatter: formatter}} = config),
    do: formatter.(message, config)

  defp format(message, _), do: message
end
