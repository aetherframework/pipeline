defmodule Aether.Pipeline.Sink.Output do
  @moduledoc """
  output sink module
  """

  alias Aether.Core.PubSub
  alias Aether.Core.Service.Resolver
  alias Aether.Pipeline.{Output, Sink}

  use Sink

  import Resolver.Process

  def init(%{options: %{key: key, default: default}}) do
    with {:ok, output} <- resolve_service(~p/output/) do
      Output.put_new(output, key, default)
    end

    {:ok, %{}}
  end

  def call(message, state, %{options: options}) do
    with {:ok, pubsub} <- resolve_service(~p/pubsub/) do
      update_output(message, pubsub, options)
    end

    {:ok, state}
  end

  def options do
    %{
      key: [type: :atom, default: :info],
      default: [type: :any, default: nil],
      update: [type: :function]
    }
  end

  defp update_output(message, pubsub, options) do
    update = fn acc -> options.update.(acc, message) end

    PubSub.dispatch(pubsub, "output:update", %{options | update: update})
  end
end
