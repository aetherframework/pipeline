defmodule Aether.Pipeline.Network do
  @moduledoc """
  Pipeline Network
  """

  alias Aether.Pipeline.Network.Server

  defdelegate deactivate(pid, opts), to: Server
  defdelegate active_links?(pid, path), to: Server
  defdelegate any_subscribers?(pid, path), to: Server
  defdelegate any_subscriptions?(pid, path), to: Server
end
