defmodule Aether.Pipeline.Stage.Producer do
  @moduledoc """
  Producer Stage
  """

  alias Aether.Pipeline.{Message, Stage}

  use Stage, type: :producer

  def produce_messages(count, stage) do
    with {:ok, items, state} <- do_produce_messages(count, stage) do
      messages = Enum.map(items, &Message.new/1)

      {:ok, messages, %{stage | state: state}}
    else
      {:error, _, stage} -> {:error, [], stage}
    end
  end

  defp do_produce_messages(count, %{config: config, state: state} = stage) do
    case config.module.produce_messages(count, state, config) do
      {:ok, messages, state} ->
        {:ok, messages, %{stage | state: state}}

      {:error, messages, state} ->
        {:error, messages, %{stage | state: state}}

      {:done, messages, state} ->
        producer_done(messages, %{stage | state: state})

      {:done, state} ->
        producer_done([], %{stage | state: state})
    end
  end

  defp producer_done(messages, stage) do
    GenStage.async_info(self(), :done)

    {:ok, messages, stage}
  end
end
