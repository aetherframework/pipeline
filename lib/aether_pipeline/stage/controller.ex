defmodule Aether.Pipeline.Stage.Controller do
  @moduledoc """
  The `Aether.Pipeline.Stage.Controller` module provides functions
  for handling the lifecycle, as well as for responding to and emitting
  events in the pipeline system, for a `Aether.Pipeline.Stage` process.
  """

  alias Aether.Core.PubSub
  alias Aether.Core.Service.Resolver
  alias Aether.Pipeline.{Logger, Message}

  import Resolver.Process

  def initialized(id, %{name: name} = config, pipeline_id) do
    with {:ok, pubsub} <- resolve_service(pipeline_id, ~p/pubsub/) do
      setup_subscriptions(pubsub, id, config)
      publish_initialized(pubsub, id, config)

      Logger.debug("[stage] #{name} stage #{id} initialized")
    end
  end

  def subscribed(from, %{config: config} = stage) do
    with {:ok, pubsub} <- resolve_service(~p/pubsub/) do
      publish_subscribed(pubsub, {from[:path], from[:stage]}, stage)

      Logger.debug("[stage] #{from[:stage]} subscribed to #{config.path}")
    end

    {:automatic, stage}
  end

  def cancelled({sub_path, :done}, %{path: path, config: config} = stage) do
    if config.path != sub_path do
      with {:ok, pubsub} <- resolve_service(~p/pubsub/) do
        publish_subscription_done(pubsub, stage, sub_path)

        Logger.debug("[stage] #{path} subscription to #{sub_path} cancelled")
      end
    end

    {:noreply, [], stage}
  end

  def flush(stage) do
    with {:ok, messages, stage} <- flush_messages(stage) do
      {:noreply, messages, stage}
    end
  end

  def done(%{type: :consumer} = stage) do
    with {:ok, pubsub} <- resolve_service(~p/pubsub/) do
      publish_done(pubsub, stage)
    end

    {:noreply, [], stage}
  end

  def done(stage) do
    with {:ok, messages, stage} <- flush_messages(stage) do
      done_msg = build_done_message(stage)
      messages = messages ++ [done_msg]

      {:noreply, messages, stage}
    end
  end

  def control_message(%Message.Control{type: :done} = message, _stage) do
    %{data: %{event: event}} = message

    with {:ok, pubsub} <- resolve_service(~p/pubsub/) do
      PubSub.dispatch(pubsub, event)
    end
  end

  defp setup_subscriptions(pubsub, _id, config) do
    subscriptions = [
      {"#{config.path}:done", {:send, :done}}
    ]

    Enum.each(subscriptions, fn {topic, handler} ->
      PubSub.subscribe(pubsub, topic, handler)
    end)
  end

  defp publish_initialized(pubsub, id, %{path: path}),
    do: PubSub.dispatch(pubsub, "#{path}:stage:init", id)

  defp publish_subscribed(pubsub, {from, stage}, %{id: id, config: config}) do
    payload = {id, from, stage}

    PubSub.dispatch(pubsub, "#{config.path}:stage:subscribed", payload)
  end

  defp publish_subscription_done(pubsub, %{id: id, config: config}, sub_path) do
    payload = {id, config, sub_path}

    PubSub.dispatch(pubsub, "#{config.path}:stage:cancelled", payload)
  end

  defp publish_done(pubsub, %{id: id, path: path, config: config}) do
    payload = {id, path, config}

    PubSub.dispatch(pubsub, "#{config.path}:stage:done", payload)
  end

  defp flush_messages(%{config: config, state: state} = stage) do
    with {:ok, messages, state} <- config.module.flush(state, config) do
      messages = Enum.map(messages, &Message.set_stage_meta(&1, stage))

      {:ok, messages, %{stage | state: state}}
    end
  end

  defp build_done_event(%{id: id, path: path, config: config}),
    do: PubSub.Event.new("#{config.path}:stage:done", {id, path, config})

  defp build_done_message(%{path: path, config: config} = stage) do
    Message.Control.new(:done,
      component: config.path,
      stage: path,
      data: %{event: build_done_event(stage)}
    )
  end
end
