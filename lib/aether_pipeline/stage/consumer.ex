defmodule Aether.Pipeline.Stage.Consumer do
  @moduledoc """
  Consumer Stage
  """

  alias Aether.Pipeline.Stage

  use Stage, type: :consumer

  def consume_messages(messages, stage) do
    stage =
      Enum.reduce(messages, stage, fn message, stage ->
        case consume_message(message, stage) do
          {:ok, state} ->
            %{stage | state: state}

          {:drop, state} ->
            %{stage | state: state}

          {:error, state} ->
            %{stage | state: state}
        end
      end)

    {:ok, stage}
  end

  defp consume_message(%Message{} = message, %{state: state, config: config}) do
    config.module.consume_message(message, state, config)
  end

  defp consume_message(%Message.Batch{} = batch, %{state: state, config: config}) do
    config.module.consume_message(batch, state, config)
  end

  defp consume_message(%Message.Control{} = message, %{state: state} = stage) do
    handle_control_message(message, stage)

    {:drop, state}
  end
end
