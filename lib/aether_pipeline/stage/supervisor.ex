defmodule Aether.Pipeline.Stage.Supervisor do
  @moduledoc """
  Stage dynamic supervisor
  """

  alias Aether.Core.Service.Resolver

  use DynamicSupervisor

  import Resolver.Process

  def start_link(config: config, pipeline_id: plid) do
    name = via_service_group(plid, ~p/#{config.path} stage supervisor/)

    DynamicSupervisor.start_link(__MODULE__, [pipeline_id: plid], name: name)
  end

  def init(pipeline_id: plid) do
    service_group_set(plid)

    DynamicSupervisor.init(
      strategy: :one_for_one,
      extra_arguments: []
    )
  end

  def start_child(supervisor, config, id, plid) do
    spec = {config.stage, id: id, config: config, pipeline_id: plid}

    DynamicSupervisor.start_child(supervisor, spec)
  end

  def start_stages(supervisor, config, pipeline_id) do
    count = Map.get(config, :stages, 1) - 1

    Enum.each(0..count, fn id ->
      start_child(supervisor, config, id, pipeline_id)
    end)
  end
end
