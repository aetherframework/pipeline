defmodule Aether.Pipeline.Stage.Processor do
  @moduledoc """
  Producer/Consumer Stage
  """

  alias Aether.Pipeline.Stage

  use Stage, type: :producer_consumer

  def process_messages(messages, stage) do
    {messages, stage} =
      Enum.reduce(messages, {[], stage}, fn message, {acc, stage} ->
        case process_message(message, stage) do
          {:drop, state} ->
            {acc, %{stage | state: state}}

          {:drop, _message, state} ->
            {acc, %{stage | state: state}}

          {:flatten, msgs, state} ->
            {msgs ++ acc, %{stage | state: state}}

          {:ok, msg, state} ->
            {[msg | acc], %{stage | state: state}}

          {:error, msg, state} ->
            {[msg | acc], %{stage | state: state}}
        end
      end)

    {:ok, Enum.reverse(messages), stage}
  end

  defp process_message(%Message{} = message, %{state: state, config: config}) do
    config.module.process_message(message, state, config)
  end

  defp process_message(%Message.Batch{} = batch, %{state: state, config: config}) do
    config.module.process_message(batch, state, config)
  end

  defp process_message(%Message.Control{} = message, %{state: state} = stage) do
    handle_control_message(message, stage)

    {:drop, state}
  end
end
