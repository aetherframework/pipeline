defmodule Aether.Pipeline.Application do
  @moduledoc false

  alias Aether.Core.Service
  alias Aether.Pipeline

  @pipelines_supervisor Pipeline.PipelinesSupervisor

  use Application

  def start(_type, _args) do
    children = [
      {Service.Supervisor, name: Service.Supervisor},
      {DynamicSupervisor, strategy: :one_for_one, name: @pipelines_supervisor}
    ]

    Supervisor.start_link(children, strategy: :rest_for_one, name: __MODULE__)
  end
end
