defmodule Aether.Pipeline.Result do
  @moduledoc """
  Pipeline Result struct
  """

  alias Aether.Core.Service.Resolver
  alias Aether.Pipeline.Output

  import Resolver

  @derive {Inspect, only: [:id, :status]}

  defstruct [:id, :status, :time, :output, :errors, :logs]

  def new(id, :success, %{start_time: start_time, end_time: end_time}) do
    time = %{start: start_time, end: end_time}
    output = get_output(id)

    %__MODULE__{
      id: id,
      status: :success,
      time: time,
      output: output
    }
  end

  def new(id, :failure, %{start_time: start_time, end_time: end_time}) do
    time = %{start: start_time, end: end_time}

    %__MODULE__{
      id: id,
      status: :failure,
      time: time,
      errors: []
    }
  end

  def success(id, data \\ %{}), do: new(id, :success, data)
  def failure(id, data \\ %{}), do: new(id, :failure, data)

  defp get_output(id) do
    with {:ok, output} <- resolve_service(id, ~p/output/) do
      Output.Server.dump(output)
    end
  end
end
