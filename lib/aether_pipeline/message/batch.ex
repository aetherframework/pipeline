defmodule Aether.Pipeline.Message.Batch do
  @moduledoc """
  Message Batch
  """

  alias Aether.Pipeline.Message

  @derive [Inspect]

  defstruct size: 0, messages: [], meta: %{}, errors: []

  @type t :: %__MODULE__{
          size: integer(),
          messages: [Message.t()],
          meta: map(),
          errors: [{String.t(), atom()}]
        }

  @default_meta %{history: []}

  def new(msgs) when is_list(msgs),
    do: %__MODULE__{size: length(msgs), messages: msgs, meta: @default_meta}

  def set_messages(%__MODULE__{} = batch, msgs) when is_list(msgs),
    do: %__MODULE__{batch | messages: msgs, size: length(msgs)}

  def add_messages(%__MODULE__{messages: msgs, size: size} = batch, new) when is_list(new),
    do: %__MODULE__{batch | messages: msgs ++ new, size: size + length(new)}

  def get_meta(%__MODULE__{meta: meta}, key),
    do: Map.get(meta, key, nil)

  def put_meta(%__MODULE__{meta: meta} = msg, key, value),
    do: %__MODULE__{msg | meta: Map.put(meta, key, value)}

  def drop_meta(%__MODULE__{} = msg, key) when not is_list(key),
    do: drop_meta(msg, [key])

  def drop_meta(%__MODULE__{meta: meta} = msg, keys),
    do: %__MODULE__{msg | meta: Map.drop(meta, keys)}

  def put_error(%__MODULE__{errors: errors} = batch, error),
    do: %__MODULE__{batch | errors: [error | errors]}

  def set_from(%__MODULE__{messages: msgs} = batch, from) do
    msgs = Enum.map(msgs, &Message.set_from(&1, from))

    batch
    |> set_messages(msgs)
    |> put_meta(:from, from)
  end

  def put_history(%__MODULE__{messages: msgs, meta: %{history: hist}} = batch, path) do
    msgs = Enum.map(msgs, &Message.put_history(&1, path))

    batch
    |> set_messages(msgs)
    |> put_meta(:history, [path | hist])
  end

  def put_channel(%__MODULE__{messages: msgs} = batch, channel) do
    msgs = Enum.map(msgs, &Message.put_channel(&1, channel))

    batch
    |> set_messages(msgs)
    |> put_meta(:channel, channel)
  end

  def drop_channel(%__MODULE__{messages: msgs} = batch) do
    msgs = Enum.map(msgs, &Message.drop_channel/1)

    batch
    |> set_messages(msgs)
    |> drop_meta(:channel)
  end

  def set_stage_meta(%__MODULE__{} = msg, %{config: config}) do
    msg
    |> set_from(config.path)
    |> put_history(config.path)
    |> set_channel(config)
  end

  defp set_channel(%__MODULE__{} = msg, %{type: :channel}),
    do: drop_channel(msg)

  defp set_channel(%__MODULE__{} = msg, %{options: %{router: router}} = config) do
    msg
    |> select_channel(router, config)
    |> (&put_channel(msg, &1)).()
  end

  defp set_channel(%__MODULE__{} = msg, %{default_channel: channel}),
    do: put_channel(msg, channel)

  defp select_channel(channel, _) when not is_nil(channel), do: channel
  defp select_channel(_, %{default_channel: channel}), do: channel

  defp select_channel(msg, router, config) when is_function(router, 1),
    do: msg |> router.() |> select_channel(config)

  defp select_channel(msg, router, config) when is_function(router, 2),
    do: msg |> router.(config) |> select_channel(config)
end
