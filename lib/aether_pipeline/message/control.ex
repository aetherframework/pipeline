defmodule Aether.Pipeline.Message.Control do
  @moduledoc """
  Pipeline Control Message
  """

  @derive [Inspect]

  defstruct [
    :type,
    :from,
    :stage,
    data: %{}
  ]

  @type t :: %__MODULE__{
          type: atom(),
          from: String.t(),
          stage: String.t(),
          data: map()
        }

  def new(:done, component: component, stage: stage, data: data),
    do: new(:done, component, stage, data)

  def new(:done, component: component, stage: stage),
    do: new(:done, component, stage, %{})

  def new(:done, stage: stage, from: from, data: data),
    do: new(:done, from, stage, data)

  def new(:done, stage: stage, from: from),
    do: new(:done, from, stage, %{})

  def new(:done, from, stage),
    do: new(:done, from, stage, %{})

  def new(:done, from, stage, data),
    do: %__MODULE__{type: :done, from: from, stage: stage, data: data}
end
