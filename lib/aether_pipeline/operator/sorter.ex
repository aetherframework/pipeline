defmodule Aether.Pipeline.Operator.Sorter do
  @moduledoc """
  Switcher Operator
  """

  alias Aether.Pipeline.Operator

  use Operator

  def init(_config), do: {:ok, %{buffer: Qex.new()}}

  def call(message, %{buffer: buffer} = state, _config) do
    buffer = Qex.push(buffer, message)

    {:drop, %{state | buffer: buffer}}
  end

  def flush(%{buffer: buffer} = state, %{options: options}) do
    messages =
      buffer
      |> Enum.to_list()
      |> sort(options)

    {:ok, messages, %{state | buffer: Qex.new()}}
  end

  def options do
    %{
      sort: [type: :function],
      sort_by: [type: :function]
    }
  end

  defp sort(messages, %{sort: sort}),
    do: Enum.sort(messages, sort)

  defp sort(messages, %{sort_by: sort_by}),
    do: Enum.sort_by(messages, sort_by)

  defp sort(messages, %{sort: sort, sort_by: sort_by}),
    do: Enum.sort_by(messages, sort_by, sort)
end
