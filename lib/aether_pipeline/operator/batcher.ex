defmodule Aether.Pipeline.Operator.Batcher do
  @moduledoc """
  Batcher Operator
  """

  alias Aether.Pipeline.{Message, Operator, Stage}

  use Operator

  def init(_config), do: {:ok, %{buffer: Qex.new()}}

  def call(msg, state, config) do
    if buffer_full?(state, config) do
      {:ok, create_batch(state), new_buffer(msg, state)}
    else
      {:drop, push_buffer(msg, state)}
    end
  end

  def flush(%{buffer: buffer} = state, %{options: %{size: size}}) do
    batches =
      buffer
      |> Enum.chunk_every(size)
      |> Enum.map(&create_batch/1)

    {:ok, batches, %{state | buffer: Qex.new()}}
  end

  def options do
    %{size: [type: :integer, default: 100]}
  end

  defp buffer_full?(%{buffer: buffer}, %{options: %{size: size}}),
    do: Enum.count(buffer) == size

  defp create_batch(%{buffer: buffer}),
    do: create_batch(buffer)

  defp create_batch(items) do
    items
    |> Enum.to_list()
    |> Message.Batch.new()
  end

  defp set_buffer(buffer, state), do: %{state | buffer: buffer}

  defp new_buffer(msg, state),
    do: set_buffer(Qex.new([msg]), state)

  defp push_buffer(msg, %{buffer: buffer} = state),
    do: set_buffer(Qex.push(buffer, msg), state)
end
