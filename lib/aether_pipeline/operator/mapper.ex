defmodule Aether.Pipeline.Operator.Mapper do
  @moduledoc """
  Mapper Operator
  """

  alias Aether.Pipeline.Operator

  use Operator

  def call(message, state, %{options: options}),
    do: {:ok, options.map.(message), state}

  def options do
    %{map: [type: :function, default: fn m -> m end]}
  end

  def default_mapper(message), do: message
end
