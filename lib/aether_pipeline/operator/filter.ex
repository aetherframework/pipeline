defmodule Aether.Pipeline.Operator.Filter do
  @moduledoc """
  Filter Operator
  """

  alias Aether.Pipeline.Operator

  use Operator

  def init(_config), do: {:ok, %{}}

  def call(message, state, %{options: options}) do
    if options.predicate.(message) do
      {:ok, message, state}
    else
      {:drop, state}
    end
  end

  def options do
    %{predicate: [type: :function, default: fn _ -> true end]}
  end
end
