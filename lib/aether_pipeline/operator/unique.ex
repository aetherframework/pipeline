defmodule Aether.Pipeline.Operator.Unique do
  @moduledoc """
  Switcher Operator
  """

  alias Aether.Pipeline.Operator

  use Operator

  def init(_config), do: {:ok, %{buffer: Qex.new()}}

  def call(message, %{buffer: buffer} = state, _config) do
    buffer = Qex.push(buffer, message)

    {:drop, %{state | buffer: buffer}}
  end

  def flush(%{buffer: buffer} = state, %{options: options}) do
    messages =
      buffer
      |> Enum.to_list()
      |> unique(options)

    {:ok, messages, %{state | buffer: Qex.new()}}
  end

  def options do
    %{
      uniq_by: [type: :function]
    }
  end

  defp unique(messages, %{uniq_by: uniq_by}),
    do: Enum.uniq_by(messages, uniq_by)

  defp unique(messages, _),
    do: Enum.uniq(messages)
end
