defmodule Aether.Pipeline.Operator.Switcher do
  @moduledoc """
  Switcher Operator
  """

  alias Aether.Pipeline.Operator

  use Operator

  def init(_config), do: {:ok, %{}}

  def call(message, state, _config), do: {:ok, message, state}

  def options do
    %{router: [type: :function, default: fn _ -> :output end]}
  end
end
