defmodule Aether.Pipeline.Executor.ComponentSupervisor do
  @moduledoc false

  alias Aether.Core.Service.Resolver
  alias Aether.Pipeline.Spec

  use DynamicSupervisor

  import Resolver.Process

  def start_link([pipeline_id: plid] = opts) do
    name = via_service_group(plid, ~p/executor components supervisor/)

    DynamicSupervisor.start_link(__MODULE__, opts, name: name)
  end

  def init(pipeline_id: plid) do
    service_group_set(plid)

    DynamicSupervisor.init(
      strategy: :one_for_one,
      extra_arguments: []
    )
  end

  def start_child(supervisor, spec),
    do: DynamicSupervisor.start_child(supervisor, spec)

  def start_components(supervisor, spec: %Spec{} = spec, pipeline_id: plid) do
    spec
    |> Spec.component_specs(plid)
    |> Enum.map(&start_child(supervisor, &1))
  end
end
