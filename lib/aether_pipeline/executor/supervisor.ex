defmodule Aether.Pipeline.Executor.Supervisor do
  @moduledoc """
  PubSub supervisor
  """

  alias Aether.Core.Service.Resolver
  alias Aether.Pipeline.Executor

  use Supervisor

  import Resolver.Process

  def start_link([spec: _, pipeline_id: plid, owner: _] = opts) do
    name = via_service_group(plid, ~p/executor supervisor/)

    Supervisor.start_link(__MODULE__, opts, name: name)
  end

  def init(spec: spec, pipeline_id: plid, owner: owner) do
    service_group_set(plid)

    children = [
      {Executor.ComponentSupervisor, pipeline_id: plid},
      {Executor.State, spec: spec, pipeline_id: plid, owner: owner},
      {Executor.Server, spec: spec, pipeline_id: plid, owner: owner}
    ]

    Supervisor.init(children, strategy: :rest_for_one)
  end
end
