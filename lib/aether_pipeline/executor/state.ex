defmodule Aether.Pipeline.Executor.State do
  @moduledoc """
  Pipeline execution state
  """

  alias Aether.Core.{PubSub, StateMachine}
  alias Aether.Core.Service.Resolver
  alias Aether.Pipeline.{Logger, Result}

  use StateMachine, restart: :transient, shutdown: 10_000

  import Resolver.Process

  @derive [Inspect]

  defstruct spec: nil,
            components: %{},
            pipeline_id: nil,
            owner: nil,
            start_time: nil,
            end_time: nil

  def start_link([spec: _spec, pipeline_id: plid, owner: _] = opts) do
    name = via_service_group(plid, ~p/executor state/)

    StateMachine.start_link(__MODULE__, opts, name: name)
  end

  def init(spec: spec, pipeline_id: plid, owner: owner) do
    service_group_set(plid)

    components = init_component_map(spec)

    data = %__MODULE__{
      spec: spec,
      components: components,
      pipeline_id: plid,
      owner: owner,
      start_time: DateTime.utc_now()
    }

    {:ok, :initializing, data}
  end

  def status(state),
    do: StateMachine.call(state, :status)

  def component_initialized(state, path),
    do: StateMachine.cast(state, {:component_init, path})

  def component_ready(state, path),
    do: StateMachine.cast(state, {:component_ready, path})

  def component_active(state, path),
    do: StateMachine.cast(state, {:component_active, path})

  def component_done(state, path),
    do: StateMachine.cast(state, {:component_done, path})

  def handle_cast({:component_init, path}, _, data) do
    data = set_component_initialized(data, path)

    {:noreply, :keep, data}
  end

  def handle_cast({:component_ready, path}, :initializing, data) do
    data = set_component_ready(data, path)

    if all_components_ready?(data) do
      {:noreply, {:next, :ready}, data, {:continue, :ready}}
    else
      {:noreply, :keep, data}
    end
  end

  def handle_cast({:component_ready, path}, _, data) do
    data = set_component_ready(data, path)

    {:noreply, :keep, data}
  end

  def handle_cast({:component_active, path}, :ready, data) do
    data = set_component_active(data, path)

    if any_components_active?(data) do
      {:noreply, {:next, :active}, data, {:continue, :active}}
    else
      {:noreply, :keep, data}
    end
  end

  def handle_cast({:component_active, path}, _, data) do
    data = set_component_active(data, path)

    {:noreply, :keep, data}
  end

  def handle_cast({:component_done, path}, :active, data) do
    data = set_component_done(data, path)

    if all_components_done?(data) do
      {:noreply, {:next, :done}, data, {:continue, :done}}
    else
      {:noreply, :keep, data}
    end
  end

  def handle_cast({:component_done, path}, _, data) do
    data = set_component_done(data, path)

    {:noreply, :keep, data}
  end

  def handle_call(:status, _from, state, _data),
    do: {:reply, state, :keep, :keep}

  def handle_continue(:ready, :ready, %{spec: spec}) do
    with {:ok, pubsub} <- resolve_service(~p/pubsub/) do
      PubSub.dispatch(pubsub, "pipeline:ready", spec)
      Logger.info("[pipeline] executor ready")
    end

    {:noreply, :keep, :keep}
  end

  def handle_continue(:active, :active, %{spec: spec}) do
    with {:ok, pubsub} <- resolve_service(~p/pubsub/) do
      PubSub.dispatch(pubsub, "pipeline:active", spec)
      Logger.info("[pipeline] executor active")
    end

    {:noreply, :keep, :keep}
  end

  def handle_continue(:done, :done, %{spec: spec, owner: owner} = data) do
    data = %__MODULE__{data | end_time: DateTime.utc_now()}

    with {:ok, pubsub} <- resolve_service(~p/pubsub/),
         {:ok, supervisor} <- resolve_service(~p/pipeline supervisor/) do
      PubSub.dispatch(pubsub, "pipeline:done", spec)

      Logger.info("[pipeline] executor done")
      Logger.info("[pipeline] starting shutdown...")

      if owner do
        result = Result.success(data.pipeline_id, data)

        send(owner, {:"$aether_pipeline:done", result})
      end

      Supervisor.stop(supervisor, :shutdown)
    end

    {:noreply, {:next, :complete}, data}
  end

  defp init_component_map(%{components: components}) do
    components
    |> Enum.map(&Map.get(&1, :path))
    |> Enum.map(fn path -> {path, :uninitialized} end)
    |> Enum.into(%{})
  end

  defp set_component_status(%{components: components} = data, key, status),
    do: %{data | components: Map.put(components, key, status)}

  defp set_component_initialized(data, key),
    do: set_component_status(data, key, :initialized)

  defp set_component_ready(data, key),
    do: set_component_status(data, key, :ready)

  defp set_component_active(data, key),
    do: set_component_status(data, key, :active)

  defp set_component_done(data, key),
    do: set_component_status(data, key, :done)

  defp all_components_have_status?(%{components: components}, status),
    do: components |> Map.values() |> Enum.all?(&(&1 == status))

  defp any_components_have_status?(%{components: components}, status),
    do: components |> Map.values() |> Enum.any?(&(&1 == status))

  defp all_components_ready?(data),
    do: all_components_have_status?(data, :ready)

  defp any_components_active?(data),
    do: any_components_have_status?(data, :active)

  defp all_components_done?(data),
    do: all_components_have_status?(data, :done)
end
