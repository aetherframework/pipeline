defmodule Aether.Pipeline.Executor.Server do
  @moduledoc """
  Pipeline execution server
  """

  alias Aether.Core.PubSub
  alias Aether.Core.Service.Resolver
  alias Aether.Pipeline.Executor.{ComponentSupervisor, State}
  alias Aether.Pipeline.Logger

  use GenServer, restart: :transient, shutdown: 10_000

  import Resolver.Process

  def start_link([spec: _, pipeline_id: plid, owner: _] = opts) do
    name = via_service_group(plid, ~p/executor/)

    GenServer.start_link(__MODULE__, opts, name: name)
  end

  def init(spec: spec, pipeline_id: plid, owner: owner) do
    service_group_set(plid)
    Process.monitor(owner)

    {:ok, {spec, plid}, {:continue, :init_pipeline}}
  end

  def state(server),
    do: GenServer.call(server, :state)

  def handle_call(:state, _from, state),
    do: {:reply, state, state}

  def handle_continue(:init_pipeline, {spec, plid}) do
    with {:ok, pubsub} <- resolve_service(~p/pubsub/) do
      setup_subscriptions(pubsub, spec)
      publish_initialized(pubsub, spec)
    end

    Logger.info("[pipeline] executor initialized")

    {:noreply, {spec, plid}, {:continue, :init_components}}
  end

  def handle_continue(:init_components, {spec, plid}) do
    with {:ok, supervisor} <- resolve_service(~p/executor components supervisor/) do
      ComponentSupervisor.start_components(
        supervisor,
        spec: spec,
        pipeline_id: plid
      )
    end

    {:noreply, {spec, plid}}
  end

  def handle_info({:component_init, %{payload: %{path: path}}}, {spec, plid}) do
    with {:ok, state} <- resolve_service(~p/executor state/) do
      State.component_initialized(state, path)
    end

    {:noreply, {spec, plid}}
  end

  def handle_info({:component_ready, %{payload: %{path: path}}}, {spec, plid}) do
    with {:ok, state} <- resolve_service(~p/executor state/) do
      State.component_ready(state, path)
    end

    {:noreply, {spec, plid}}
  end

  def handle_info({:component_active, %{payload: %{path: path}}}, {spec, plid}) do
    with {:ok, state} <- resolve_service(~p/executor state/) do
      State.component_active(state, path)
    end

    {:noreply, {spec, plid}}
  end

  def handle_info({:component_done, %{payload: %{path: path}}}, {spec, plid}) do
    with {:ok, state} <- resolve_service(~p/executor state/) do
      State.component_done(state, path)
    end

    {:noreply, {spec, plid}}
  end

  defp setup_subscriptions(pubsub, _spec) do
    subscriptions = [
      {"component:init", {:send, :component_init}},
      {"component:ready", {:send, :component_ready}},
      {"component:active", {:send, :component_active}},
      {"component:done", {:send, :component_done}}
    ]

    Enum.each(subscriptions, fn {topic, handler} ->
      PubSub.subscribe(pubsub, topic, handler)
    end)
  end

  defp publish_initialized(pubsub, spec),
    do: PubSub.dispatch(pubsub, "pipeline:init", spec)
end
