defmodule Aether.Pipeline.Component do
  @moduledoc """
  Pipeline Component
  """

  alias Aether.Core.Service
  alias Aether.Pipeline.Component.Server

  @type path :: Service.path()

  @callback build(opts :: term()) :: term()
  @callback options() :: map()

  @callback produce_messages(count :: integer(), state :: term(), config :: term()) :: [term()]
  @callback process_message(message :: term(), state :: term(), config :: term()) :: [term()]
  @callback consume_message(message :: term(), state :: term(), config :: term()) :: :ok | :error

  @callback component_spec(config :: term(), pipeline_id :: binary()) :: term()
  @callback dependencies_specs(config :: term(), pipeline_id :: binary()) :: term()

  @optional_callbacks produce_messages: 3,
                      process_message: 3,
                      consume_message: 3,
                      dependencies_specs: 2

  defdelegate stages(pid), to: Server
  defdelegate connection_opts(pid), to: Server

  defmacro __using__(type: type, stage: stage) do
    quote location: :keep, bind_quoted: [type: type, stage: stage] do
      alias Aether.Pipeline.Component

      @behaviour Component

      @impl Component
      def build(opts) do
        option_keys = Map.keys(options())
        config_opts = Map.drop(opts, option_keys)
        module_opts = Map.take(opts, option_keys)

        config =
          config_opts
          |> Map.put(:type, unquote(type))
          |> Map.put(:stage, unquote(stage))
          |> Map.put(:options, module_opts)
          |> Component.Config.new()

        {:ok, config}
      end

      @impl Component
      def options, do: %{}

      @impl Component
      def produce_messages(_count, state, _config),
        do: {:ok, [], state}

      @impl Component
      def process_message(message, state, _config),
        do: {:ok, message, state}

      @impl Component
      def consume_message(_message, state, _config),
        do: {:ok, state}

      @impl Component
      def component_spec(config, pipeline_id),
        do: Component.component_spec(config, pipeline_id)

      @impl Component
      def dependencies_specs(_config, _pipeline_id), do: []

      defoverridable build: 1,
                     options: 0,
                     produce_messages: 3,
                     consume_message: 3,
                     process_message: 3,
                     component_spec: 2,
                     dependencies_specs: 2
    end
  end

  def component_spec(config, pipeline_id) do
    opts = [config: config, pipeline_id: pipeline_id]

    %{
      id: config.name,
      type: :supervisor,
      start: {__MODULE__.Supervisor, :start_link, [opts]},
      restart: :transient
    }
  end
end
