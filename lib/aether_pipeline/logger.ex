defmodule Aether.Pipeline.Logger do
  @moduledoc """
  Pipeline Logger
  """

  alias Aether.Core.{PubSub, Service}

  import Service.Resolver.Process

  def log(level, message, opts \\ [])
  def log(:debug, message, opts), do: debug(message, opts)
  def log(:info, message, opts), do: info(message, opts)
  def log(:warn, message, opts), do: warn(message, opts)
  def log(:error, message, opts), do: error(message, opts)

  def debug(message, opts \\ [])

  def debug(message, [pubsub: pubsub] = opts) when is_pid(pubsub),
    do: message |> log_event(:debug, opts) |> do_log(pubsub)

  def debug(message, [pipeline_id: plid] = opts),
    do: message |> log_event(:debug, opts) |> do_log(plid)

  def debug(message, opts),
    do: message |> log_event(:debug, opts) |> do_log()

  def info(message, opts \\ [])

  def info(message, [pubsub: pubsub] = opts) when is_pid(pubsub),
    do: message |> log_event(:info, opts) |> do_log(pubsub)

  def info(message, [pipeline_id: plid] = opts),
    do: message |> log_event(:info, opts) |> do_log(plid)

  def info(message, opts),
    do: message |> log_event(:info, opts) |> do_log()

  def warn(message, opts \\ [])

  def warn(message, [pubsub: pubsub] = opts) when is_pid(pubsub),
    do: message |> log_event(:warn, opts) |> do_log(pubsub)

  def warn(message, [pipeline_id: plid] = opts),
    do: message |> log_event(:warn, opts) |> do_log(plid)

  def warn(message, opts),
    do: message |> log_event(:warn, opts) |> do_log()

  def error(message, opts \\ [])

  def error(message, [pubsub: pubsub] = opts) when is_pid(pubsub),
    do: message |> log_event(:error, opts) |> do_log(pubsub)

  def error(message, [pipeline_id: plid] = opts),
    do: message |> log_event(:error, opts) |> do_log(plid)

  def error(message, opts),
    do: message |> log_event(:error, opts) |> do_log()

  defp log_event(message, level, opts) do
    PubSub.Event.new(:log, %{
      level: level,
      message: message,
      metadata: opts
    })
  end

  defp do_log(event) do
    with {:ok, pubsub} <- resolve_service(~p/pubsub/) do
      do_log(event, pubsub)
    end
  end

  defp do_log(event, pipeline_id) when is_binary(pipeline_id) do
    with {:ok, pubsub} <- resolve_service(pipeline_id, ~p/pubsub/) do
      do_log(event, pubsub)
    end
  end

  defp do_log(event, pubsub) when is_pid(pubsub),
    do: PubSub.dispatch(pubsub, event)
end
