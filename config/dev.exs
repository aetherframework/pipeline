use Mix.Config

config :git_hooks,
  verbose: true,
  hooks: [
    pre_commit: [
      tasks: [
        "mix format --check-formatted"
      ]
    ]
  ]
