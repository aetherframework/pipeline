defmodule Aether.Pipeline.MixProject do
  @moduledoc false

  use Mix.Project

  def project do
    [
      app: :aether_pipeline,
      version: "0.1.0",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_coverage: [tool: ExCoveralls],
      consolidate_protocols: Mix.env() != :test,
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.detail": :test,
        "coveralls.post": :test,
        "coveralls.html": :test
      ],
      name: "Aether Pipeline",
      source_url: "https://gitlab.com/aetherframework/pipeline",
      homepage_url: "https://gitlab.com/aetherframework/pipeline",
      docs: [
        # The main page in the docs
        main: "Aether Pipeline",
        formatter_opts: [gfm: true],
        extras: ["README.md"]
      ]
    ]
  end

  def application do
    [
      extra_applications: [:logger, :timex],
      mod: {Aether.Pipeline.Application, []}
    ]
  end

  defp deps do
    [
      {:aether_core, git: "https://gitlab.com/aetherframework/core.git"},
      {:gen_stage, "~> 0.14"},
      {:libgraph, "~> 0.13.3"},
      {:nanoid, "~> 2.0.1"},
      {:qex, "~> 0.5"},
      {:timex, "~> 3.1"},
      {:uuid, "~> 1.1"},
      {:benchee, "~> 1.0", only: :dev},
      {:credo, "~> 1.0.0", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 0.5", only: [:dev, :test], runtime: false},
      {:git_hooks, "~> 0.3.0", only: :dev, runtime: false},
      {:excoveralls, "~> 0.10", only: :test}
    ]
  end
end
